<?php


header('Content-type: application/json');
$datos = new PDO("mysql:dbname=BPXPORT;host:127.0.0.1","root","eldeorejo");

$accion=(isset($_GET['accion']))?$_GET['accion']:'leer';
switch($accion){
    case 'agregar':
        //comprobamos si existe ya un registro igual al que se quiere agregar
        $fecha_actividad= $_POST['start'];
        $actividad = $_POST['title'];
      
        $sql_duplicados = $datos->prepare("SELECT start,title FROM ACTIVIDADES WHERE start = '$fecha_actividad' AND title = '$actividad'");
        $sql_duplicados->execute();
        $resultado = $sql_duplicados->fetchAll(PDO::FETCH_NUM);
        if($resultado){
            break;
        }
        //////////////////////////////////
        
        $sql = $datos->prepare("INSERT INTO 
                ACTIVIDADES(title,descripcion,start,end,color,textcolor,plazas,monitor)
                VALUES(:title,:descripcion,:start,:end,:color,:textcolor,:plazas,:monitor)");
        
        $respuesta=$sql->execute(array(
            "title" => $_POST['title'],
            "descripcion" => $_POST['descripcion'],
            "start" => $_POST['start'],
            "end" => $_POST['end'],
            "color" => $_POST['color'],
            "textcolor" => $_POST['textcolor'],
            "plazas" => $_POST['plazas'],
            "monitor" => $_POST['monitor'],
        ));
        
        echo json_encode($respuesta); 
        break;
    case 'eliminar':
       $respuesta = false;
        if(isset($_POST['id'])){
            $sql = $datos->prepare("DELETE FROM ACTIVIDADES WHERE id = :ID");
            $respuesta = $sql->execute(array("ID"=>$_POST['id']));
        }
        echo json_encode($respuesta); 
        break;
    case 'modificar':
        $sql = $datos->prepare("UPDATE ACTIVIDADES SET
                                
                                title=:title,
                                descripcion=:descripcion,
                                start=:start,
                                end=:end,
                                color=:color,
                                textcolor=:textcolor,
                                plazas=:plazas,
                                monitor=:monitor
                                WHERE id = :ID");
        $respuesta=$sql->execute(array(
            "ID" => $_POST['id'],
            "title" => $_POST['title'],
            "descripcion" => $_POST['descripcion'],
            "start" => $_POST['start'],
            "end" => $_POST['end'],
            "color" => $_POST['color'],
            "textcolor" => $_POST['textcolor'],
            "plazas" => $_POST['plazas'],
            "monitor" => $_POST['monitor'],
        ));
        echo json_encode($respuesta); 
        break;    
    

        default:
            $sql = $datos->prepare("SELECT * FROM ACTIVIDADES");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
            echo json_encode($resultado);
        break;
}




?>
