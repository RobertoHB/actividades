<?php
include("funciones.php");
ini_set("session.gc_maxlifetime", 60);
session_start();
$nombre_usuario= $_SESSION['usuario'];
$tipo_usuario= $_SESSION['tipo'];
if($nombre_usuario == '' || $nombre_usuarioombre = null){
    echo("No se inicio sesion");
    //die();
    header('Location: login.php');
}
?>


<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewpoort" content="width=device-width,user-scalable=no, initial-scale=1.0,
          maximum-scale=1.0, minimum-scale=1.0">
    <!--librerias jquery-->
     <!--<script src="http://code.jquery.com/jquery-1.9.1.js"></script>-->
    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="librerias/javascript/jquery.easy-autocomplete.min.js"></script> 
     <!--css que contiene las CSS del autocomplete-->
     <link rel="stylesheet" href="librerias/css/easy-autocomplete.css">
     <link rel="stylesheet" href="librerias/css/easy-autocomplete.themes.css">
     
   <!--librerias bootstrap-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    
    
    <!--<script type="text/javascript" src="librerias/javascript/jquery.js"></script>-->
     <link rel="stylesheet" href="estilos/estilos.css">
     
     <!--css que contiene las fuentes e iconos de la app-->
     <link rel="stylesheet" href="css/fontawesome-all.min.css">
     
     
     
   
      
     
    <script src="librerias/javascript/moment.min.js"></script>

    <!--full-calendar-->
    <link rel="stylesheet" href="librerias/css/fullcalendar.min.css">
    <script src="librerias/javascript/fullcalendar.min.js"></script>  
    <script src="librerias/javascript/es.js"></script> 
    <!--libreria js-->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    
        <!--autocomplete-->
   

    <script language="JavaScript" SRC="funciones.js"></script>
   <!--ClockPickerr-->
   <link rel="stylesheet" href="librerias/css/bootstrap-clockpicker.css">
   <script src="librerias/javascript/bootstrap-clockpicker.js"></script>  
    
<style>
label {color: black;}
</style>      
    
</head>

<body>
    
    <header>
<!--        <nav class="menu">
            <div class="container-menu">
                 <div class="logo">
                     <div class="logo-name">
                         <img src="imagenes/logo_bpx.png" alt="">
                         <a href="#">Administrador De Actividades</a>
                     </div>
                     <div class="icon-menu">
                         <a href="#" id="btn-menu" class="btn-menu"><span class="fa fa-bars"></span></a>
                        
                     </div>   
                      
                </div>
            </div>
            <div class="menu-link">
                <ul id="menu-ul">
                <ul>
                    <li><a href="#">APP'S</a></li>

                    <li><a href="#">SOCIOS</a></li>
                    <li><a href="#">RESERVAS</a></li>
                    <li><a href="#">INFORMES</a> </li>
               
                    <li>
                        <a href="#"><?php echo $nombre_usuario ?></a>
                        <span class="fa fa-user-friends"></span>
                            <ul>
                                <li><a href="#">Editar usuario</a></li>
                                <li> <a href="#">Cerrar Sesión</a></li>
                            </ul>
                   </li>
                </ul>
                
                
            </div>
            
        </nav>-->
        
            <div class="container-menu">
                                
                    <div class="logo">
                         <div class="logo-name col-sm-8">
                             <img src="imagenes/logo_bpx.png" alt="" style="border-radius:5px;"/>
                             <label id="lab-tit-logo">Administración de Actividades</label>

                         </div>
                         <div class="icon-menu col-sm-1">
                             <a href="#" id="btn-menu" class="btn-menu"><span class="fa fa-bars"></span></a>

                         </div>
                         <div class="icon-usuario col-sm-3">
<!--                            <ul id="usuario-ul"> 
                                <ul>
                                   <li>-->
                                       <a href="#"><?php echo $nombre_usuario ?><span class="fa fa-user-friends"></span></a><a href="cerrar_sesion.php"><span class="fa fa-power-off" title="Desconectar"></span></a>
<!--                                       <ul>
                                            <li> <a href="cerrar_sesion.php" >Cerrar Sesión</a></li>
                                        </ul>-->
<!--                                   </li>
                                </ul>
                            </ul>    -->
                         </div>    
                     </div>  
               
            </div>
        <?php if($tipo_usuario == 'Administrador'){ ?>
        <nav class="menu">
            
            <div class="menu-link">
                <ul id="menu-ul">
<!--                <ul>-->
                    
                    <li><a href="admin_actividad.php">CALENDARIO</a></li>

                    <li><a href="#" onclick="cargar_socios()">SOCIOS</a></li>
                    <li><a href="#" onclick="cargar_reservas()">RESERVAS</a></li>
                    <li><a href="#">MANTENIMIENTO</a><span class="fa fa-caret-down" style="float:left;clear:both;padding-left: 100;"></span>
                        <ul id="menu-mante">
                            <li><a href="#" onclick="subir_fichero()">Actualizar ficheros</a></li>
                            <li><a href="#" onclick="cargar_usuarios()">usuarios</a></li>
                            <li><a href="#" onclick="ventana_comunicacion()">Comunicación</a></li>
                            <li><a href="#">Horarios</a></li>
                        </ul>
                    </li>       
                    <li><a href="#">INFORMES</a> </li>
                     <?php } ?>
<!--                    <li>
                        <a href="#"><?php echo $nombre_usuario ?></a>
                        <span class="fa fa-user-friends"></span>
                            <ul>
                                <li><a href="#">Editar usuario</a></li>
                                <li> <a href="#">Cerrar Sesión</a></li>
                            </ul>
                   </li>-->
                </ul>
                
                
            </div>
            
        </nav>




   
    </header>
    
    <div class="filtro-activi">
   <label>Filtrar</label>           
        <select id="actividades_selector" name="actividades_selector" style="margin-left:5px;">       
        <?php
        $conectando = conectar();
        $sql = "SELECT * FROM BPXPORT.ACTIVIDADES GROUP BY title";
        $consulta = mysqli_query($conectando, $sql);
         echo "(<option value='TODAS'>TODAS</option >'.'<br>')";
         while($row=mysqli_fetch_array($consulta)){
             $valor = $row['title'];
            echo "(<option value='$valor'>$valor</option >'.'<br>')";
         }
         ?>
        </select>
         <select id="monitor_selector" name="monitor_selector" style="margin-left:5px;">       
        <?php
        $conectando = conectar();
        $sql = "SELECT monitor FROM BPXPORT.ACTIVIDADES GROUP BY monitor";
        $consulta = mysqli_query($conectando, $sql);
         echo "(<option value='TODOS'>TODOS</option >'.'<br>')";
         while($row=mysqli_fetch_array($consulta)){
             $valor = $row['monitor'];
            echo "(<option value='$valor'>$valor</option >'.'<br>')";
         }
         ?>
        </select>
</div> 
    
    <!--<div id="resultadoBusqueda" style="color:black"></div>-->
    <section class="banner">
        <div class="container" style="margin-top: 15px;color:black;float:left;clear:both">
            <div class="row" style="max-width:100%">
                <!--<div class="col"></div>-->
                <div class="col-12" id="columnas_calendario"> <div id="calendario_actividades"></div></div>
                <!--<div class="col"></div>-->
            </div>
        </div>
        
    </section>
    <script>
        $(document).ready(function(){
        //filtro = $('select[name="actividades_selector"] option:selected').text();
        
            $('#calendario_actividades').fullCalendar({
                height: 650,
                width: 1900,
                defaultView: 'month',
                header:{
                    left:'today,prev,next',
                    center:'title',
                    right:'month,listWeek'
                },
               
                views: { // set the view button names
                    listWeek: {buttonText: 'Diario'}

                 },
//                customButtons:{
//                    Miboton:{
//                        text:'Boton 1',
//                        click:function(){
//                            alert("Accion del boton");
//                            
//                        }
//                    }
//                },
                
                dayClick:function(date,jsEvent,view){
//                    
                    $("#txtFecha").val(date.format());
//                    
                    $("#txtActividad").html(jsEvent.title);
                    $("#txtMoniSala").html(jsEvent.descripcion);
                    $('#txtPlazas').val(jsEvent.plazas);
                    $('#txtMonitor').val(jsEvent.monitor);  
                    $("#txtColor").html(jsEvent.color); 
                     <?php if($tipo_usuario == 'Administrador'){ ?>
                         $("#int_actividad").modal();
                     <?php }else{ ?>
                        
                         alert("Dia seleccionado sin Actividad.");
                    <?php } ?>
                    
                    },
                events:'actividades.php',
                eventClick:function(calEvent,jsEvent,view){
//                    
//                    
                    $('#txtID').val(calEvent.id);
                    $('#txtActividad').val(calEvent.title);
                    $('#txtMoniSala').val(calEvent.descripcion);
                    $('#txtPlazas').val(calEvent.plazas);
                    $('#txtMonitor').val(calEvent.monitor);
                    
                    $('#txtColor').val(calEvent.color); 
                     //En la variable FechaHora se divide con split porr un lado la fecha [0](recoge hasta el primer espacio) y por otro la hora [1](recoge despues del espacio)
                    FechaHora = calEvent.start._i.split(" ");
                   
                  
                    $('#txtFecha').val(FechaHora[0]);
                    $('#txtHora').val(FechaHora[1]);
                    
                     <?php if($tipo_usuario == 'Administrador'){ ?>
                        $('#txtID_Actividad').val(calEvent.id);
                         $('#titulo_actividad_listado').val(calEvent.title);
                         $('#fecha_actividad_listado').val(FechaHora);
                         $('#fondo_color_titulo').val(calEvent.color);    
                         $("#int_actividad").modal();
                     <?php }else{ ?>
                         $('#txtID_Actividad').val(calEvent.id);
                         $('#titulo_actividad_listado').val(calEvent.title);
                         $('#fecha_actividad_listado').val(FechaHora);
                         $('#fondo_color_titulo').val(calEvent.color);
                         
                         $("#listo_actividad").modal();
                         listado_reservas_monitor(calEvent.color);
                         
                    <?php } ?>
                   
                       
//                    
                },
//                eventRender: function(event, element) { 
//                  element.find('.fc-title').append("<br/>" + event.descripcion); 
//                } 
                eventRender: function(event, element, view) { 
    //              element.find('.fc-title').append("<br/>" + event.descripcion); 
                        if ($("#actividades_selector").val() != 'TODAS'){
                            return['TODAS',event.title].indexOf($("#actividades_selector").val()) >=0     
                        }
                         if ($("#monitor_selector").val() != 'TODOS'){
                            return['TODOS',event.monitor].indexOf($("#monitor_selector").val()) >=0
                        }
                }
               
                      
                
                
            
                
                
            });
            $("#actividades_selector").change(function(){    
                $('#calendario_actividades').fullCalendar('rerenderEvents');   
             });
            $("#monitor_selector").change(function(){    
                $('#calendario_actividades').fullCalendar('rerenderEvents');   
             }); 
            $('#btn-menu').click(function(){
                if ( $ ('.btn-menu span').attr('class')== 'fa fa-bars'){
                    $('.btn-menu span').removeClass('fa fa-bars').addClass('fa fa-times');
                    $('.menu-link').css({'left':'0%'});
                    $('.banner').css({'display':'none'});
                    $(".filtro-activi").hide();
                }else{
                    $('.btn-menu span').removeClass('fa fa-times').addClass('fa fa-bars');
                    $('.menu-link').css({'left':'-100%'});
                    $('.banner').show();
                }
            });
            
        });        
        
    </script>    
  
    <!--modal para insertar,modificar,eliminar,reservar actividades-->
   
    <div class="modal fade" id="int_actividad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <!--<h5 class="modal-title" id="tituloEvento" style="color:black">Seleccionar Actividad</h5>-->
              
            <div class="modal-title">
                <div class="row">
                    <div class="col-sm-6" id="div_actividades" style="max-width: 100%">   
                        <label>Actividad</label>
                        <select id="copia_actividades_selector" name="copia_actividades_selector" style="width: 280px;" onchange="relleno_modal_act(this)">       
                        <?php
                        $conectando = conectar();
                        $sql = "SELECT id,title,descripcion,start,color,monitor,plazas FROM BPXPORT.ACTIVIDADES,(SELECT Max(id) as ultimo
                                FROM BPXPORT.ACTIVIDADES
                                GROUP BY title order by ultimo asc) as grupo
                                Where ACTIVIDADES.id=grupo.ultimo";
                        $consulta = mysqli_query($conectando, $sql);
                         echo "(<option value=''></option >'.'<br>')";
                         while($row=mysqli_fetch_array($consulta)){
                            $sel_actividad = $row['title'];
                            $sel_desc = $row['descripcion'];
                            $sel_color = $row['color'];
                            $sel_plazas = $row['plazas'];
                            $sel_monitor =$row['monitor'];
                            echo "(<option value='$sel_actividad' data-desc='$sel_desc' data-col='$sel_color' data-plazas='$sel_plazas' data-monitor='$sel_monitor'>$sel_actividad</option>'.'<br>')";
                         }
                         ?>
                        </select>
                     </div>
                    <div class="col-sm-4" id="div_socios" style="max-width: 100%;display:none;">      
                        <label>Socio</label>         
                        <input id="sel_socio"><input id="codigo_socio" Type="hidden">
                    </div>
                    <div class="col-sm-2" id="ico_reserva_socio" style="display:none;">
                        <span class="fa fa-check" id="ico_valida_reserva" onclick="verificar_autenti('recepcion')">
                    </div>
                </div>
                <script type="text/javascript">
                    var options= {
                        url:"autocomplete_socios.php",
                    
                    getValue: function(element) {
                    return element.APELLIDOS + " " + element.NOMBRE + " " + element.CODIGO;
                   
                    },   
                   
                    list:{
                        match:{
                            enabled:true
                        },
                        onClickEvent: function() {
                           $('#ico_reserva_socio').show();
                           $('#ico_valida_reserva').css("color", "green");
                          
                           
                        },
                        onSelectItemEvent: function() {
                            var value = jQuery( "#sel_socio" ).getSelectedItemData().CODIGO;
                            jQuery("#codigo_socio").val(value); //copy it to the hidden field
                        },
                       
                        
                        maxNumberOfElements:4
                    },
                    theme:"plate-dark"
                };
                $("#sel_socio").easyAutocomplete(options);
                </script>

            </div>
              
            <!--<h5 class="modal-title" id="titulosocio" style="color:black">Seleccionar Socio</h5>-->
              
         
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="vaciar_modal()">
              <span aria-hidden="true">&times;</span>
            </button>
             
          </div>
          <div class="modal-body">
              <input type="hidden" id="txtID" name="txtID" />
              <input type="hidden" id="txtFecha" name="txtFecha" />
              <div class="form-row">
                <div class="form-group col-sm-8">
                     <label>Actividad</label>
                     <input type="text" id="txtActividad" name="txtActividad" class="form-control" placeholder="Actividad" />
                </div>
                <div class="form-group col-sm-4">
                     <label>Hora</label>
                     <div class="input-group clockpicker" data-autoclose="true">
                        <input type="text" id="txtHora" value="09:00" class="form-control"/>
                     </div>
                </div>
              </div>  
              <div class="form-row">
                <div class="form-group col-sm-4">   
                    <label>Sala</label>
                    <input id="txtMoniSala" name="txtMoniSala" class="form-control"/>     
                </div>
                <div class="form-group col-sm-4">   
                    <label>Monitor</label>
                    <input id="txtMonitor" name="txtMonitor"  class="form-control"/>    
                </div>
                <div class="form-group col-sm-4">   
                    <label>Plazas</label>
                    <input type="text" id="txtPlazas" name="txtPlazas" class="form-control"/>     
                </div>
              </div>       
                <div class="form-group">   
                    <label>Color</label>  
                    <input type="color" value="#ff0000" id="txtColor" name="txtColor" style="height:36px" class="form-control" />
                </div>   
            </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" id="btnlistado" >Listado</button>
            <button type="button" class="btn btn-success" id="btnreservar" >Reservar</button>
            <button type="button" class="btn btn-success" id="btnAgregar">Agregar</button>
            <button type="button" class="btn btn-success" id="btnModificar">Modificar</button>
            <button type="button" class="btn btn-danger" id="btnEliminar">Borrar</button>
            <!--<button type="button" class="btn btn-default" data-dismiss="modal" onclick="vaciar_modal()">Cancelar</button>-->
          </div>
        </div>
      </div>
    </div>
    
     
    
    <script>
//    La sacamos fuera para poder utilizarla en todo momento 
    var NuevoEvento  
        $("#btnAgregar").click(function(){
            RecolectarDatosGUI();
            EnviarInformacion('agregar',NuevoEvento);
        });
        $("#btnEliminar").click(function(){
            RecolectarDatosGUI();
            EnviarInformacion('eliminar',NuevoEvento);
        });
         $("#btnModificar").click(function(){
            RecolectarDatosGUI();
            EnviarInformacion('modificar',NuevoEvento);
        });
         $("#btnreservar").click(function(){
            $('#div_socios').show();
           
        });
        $("#btnlistado").click(function(){
            var color_fondo = $('#txtColor').val(); 
            $("#listo_actividad").modal();
            listado_reservas_monitor(color_fondo);
           
        });
        
    function RecolectarDatosGUI(){
        NuevoEvento = {
               id:$('#txtID').val(),
               title:$('#txtActividad').val(),
               start:$('#txtFecha').val()+" "+$('#txtHora').val(),
               end:$('#txtFecha').val()+" "+$('#txtHora').val(),
               descripcion:$('#txtMoniSala').val(),
               color:$('#txtColor').val(),
               textcolor:"#FFFFFF",
               plazas:$('#txtPlazas').val(),
               monitor:$('#txtMonitor').val()
           };
           
          
//           if (this.NuevoEvento && this.NuevoEvento[start + title]) { return; } 
//alert(this.NuevoEvento.title+this.NuevoEvento.start;)
        
    }
    function EnviarInformacion(accion,objEvento){
        $.ajax({
           type:'POST',
           url:'actividades.php?accion='+accion,
           data:objEvento,
           success:function(msg){
               
               if(msg){
                  
                    $('#calendario_actividades').fullCalendar('refetchEvents')
                    $("#int_actividad").modal('toggle'); 
                    

               }
           },
           
           error:function(){
               
               alert("Error: Actividad duplicada");
               
           }
           
        });
    }
    function relleno_modal_act(valor){
//        var seleccionado = $(this).find('option:selected');
      
//       var actividad = seleccionado.data('value');
        $('#txtActividad').val($('option:selected', valor).attr("value"));
        $('#txtMoniSala').val($('option:selected', valor).attr("data-desc"));
        $('#txtColor').val($('option:selected', valor).attr("data-col"));
        $('#txtPlazas').val($('option:selected', valor).attr("data-plazas"));
         $('#txtMonitor').val($('option:selected', valor).attr("data-monitor"));      
                
    }
    function listado_reservas_monitor(color_fondo){
        var codigo_actividad = $("#txtID_Actividad").val();
        var color = color_fondo;
       
        $('.fondo_titulo').css('background-color',color_fondo);
     


        
        $.post('ajax.php', {reservas_Actividad: codigo_actividad,color:color}, function(mensaje) { 
          
          
            $('#lista_reservas_moni').html(mensaje);
             
          });
    }
    //Ponemos a la clase clockpicker el selector clockpicker
        $('.clockpicker').clockpicker();
    </script>     
    <script>
        function cargar_socios(){
           if ( $ ('.btn-menu span').attr('class')== 'fa fa-times'){
                    $('.btn-menu span').removeClass('fa fa-times').addClass('fa fa-bars');
                    $('.menu-link').css({'left':'-100%'});
                    $('.banner').show();
            }
            $("#calendario_actividades").load('socios.php');
            $(".filtro-activi").hide();
            $('.container').css('margin-top',120);
        }
        function cargar_usuarios(){
           
            $("#calendario_actividades").load('usuarios.php');
             $(".filtro-activi").hide();
            $('.container').css('margin-top',120);
        }
        function cargar_reservas(){
            $("#calendario_actividades").load('reservas.php');
            $(".filtro-activi").hide();
             $('.container').css('margin-top',150);
        }
        function subir_fichero(){       
            window.open('subir_fichero_socios.php','_blank','width=450,height=300,top=350px,left=450px'); 
            $("#modal_subida_ficheros").modal('toggle');      
        }
        function ventana_comunicacion(){
             window.open('comunicacion.php','_blank','width=800,height=400,top=200px,left=200px');
        }
    </script>
     <div class="modal fade" id="listo_actividad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
          
        <div class="modal-content">
          <div class="modal-header">
            
            <div class="modal-title"> 
                <div class="fondo_titulo">
                    <input class="titulo_list_moni" id="titulo_actividad_listado" name="titulo_actividad_listado" />
                    <input class="fecha_list_moni" id="fecha_actividad_listado" name ="fecha_actividad_listado" />
                    <input class="fondo_list_moni" id="fondo_color_titulo" type="hidden" />
                    <input id="txtID_Actividad" name="txtID_Actividad" type="hidden" />
                </div>
            </div>
            
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="">
              <span aria-hidden="true">&times;</span>
            </button>
             
          </div>
        <!--<div class="modal-body">-->
 
            <div class="modal-body" id="lista_reservas_moni" name="lista_reservas_moni"></div>  
                               
        <!--</div>-->
          
            
          </div>
        </div>
      </div>
    
    
    
</body>
</html>

