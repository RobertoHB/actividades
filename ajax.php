    <?php
include("funciones.php");


if (isset($_POST['valorusuario'])){
   
    //Conectamos con la bd de usuarios
    $conectando = conectar();
    
    $valorusuario=$_POST['valorusuario'];
    $valorcontra=$_POST['valorcontra']; 
    
    $sql = "SELECT * FROM BPXPORT.USUARIOS where NOMBRE = '$valorusuario' AND CONTRA = '$valorcontra'";
    
    $logeado = mysqli_query($conectando, $sql);
    $fila = mysqli_fetch_row($logeado);
    
    if(mysqli_num_rows($logeado) > 0){
        session_start();
        $_SESSION['usuario']= $valorusuario; 
        $_SESSION['tipo']= $fila[3]; 
       //header ("Location: admin_actividad.php"); 
       //include('admin_actividad.php');
        
        echo("1");
    }else{
         echo("0");
       
    }
}

if (isset($_POST['valornif'])){
   
    //Conectamos con la bd de socios
    $conectando = conectar();
    
    $valornif = $_POST['valornif'];
    $valormovil = $_POST['valormovil']; 
    
    
    $sql = "SELECT * FROM BPXPORT.SOCIOS where NIF = '$valornif' AND MOVIL = '$valormovil' AND (FECHA_BAJA = NULL or FECHA_BAJA = 0)";
    
    $logeado = mysqli_query($conectando, $sql);
    
    $fila = mysqli_fetch_row($logeado);
    
    if(mysqli_num_rows($logeado) > 0){
        session_start();
        $_SESSION['NOMBRE']= $fila[3];
        $_SESSION['APELLIDOS']= $fila[4];
        $_SESSION['CODIGO']= $fila[1];
       //header ("Location: admin_actividad.php"); 
       //include('admin_actividad.php');
        
        echo("1");
    }else{
         echo("0");
       
    }
}
if (isset($_POST['valorpulsera'])){
     //Conectamos con la bd de socios
    $conectando = conectar();
    $tipo = $_POST['tipo'];
    $valorpulsera = $_POST['valorpulsera'];
    //$valorpulsera = ltrim($valorpulsera,"0");
    $valoralternativo = $_POST['valoralternativo']; 
    if($tipo === 'lector'){
        $sql = "SELECT * FROM BPXPORT.SOCIOS where CODIGO_PUL = '$valorpulsera'";
        $logeado = mysqli_query($conectando, $sql);
        $fila = mysqli_fetch_row($logeado);
        if(mysqli_num_rows($logeado) > 0){
            session_start();
            $_SESSION['NOMBRE']= $fila[3];
            $_SESSION['APELLIDOS']= $fila[4];
            $_SESSION['CODIGO']= $fila[1];
            echo("1");
        }else{
            
            //si no existe el codigo de pulsera mensaje e identificacion por codigo o nif. 
            //Guardamos el codigo de pulsera en variable de sesion para insertarla en la bd despues de valiar.
            echo("Codigo de pulsera no registrado. Identifiquese por código de abonado o NIF");
            session_start();
            $_SESSION['guardar_codpul']= $valorpulsera;
        }
    }
    if($tipo === 'teclado'){
    
    $sql = "SELECT * FROM BPXPORT.SOCIOS WHERE CODIGO = '$valorpulsera' OR NIF = '$valorpulsera'";
   
    
    $logeado = mysqli_query($conectando, $sql);
    
    $fila = mysqli_fetch_row($logeado);
    
        if(mysqli_num_rows($logeado) > 0){
            session_start();
            $_SESSION['NOMBRE']= $fila[3];
            $_SESSION['APELLIDOS']= $fila[4];
            $_SESSION['CODIGO']= $fila[1];
            $codio_pulsera= $fila[2];
            $codigo_pul_leido =  $_SESSION['guardar_codpul'];
            if($codio_pulsera == '' OR $codio_pulsera == '0'){
                //insertamos en nuestra bd el codigo de pulsera que guardamos en la sesion por rfid
                $sql = "UPDATE BPXPORT.SOCIOS SET CODIGO_PUL = '$codigo_pul_leido' WHERE CODIGO = '$fila[1]'";
                $resultado= mysqli_query($conectando, $sql);
                
            } 
            echo("1");
           //header ("Location: admin_actividad.php"); 
           //include('admin_actividad.php');

        }else{
            echo("0");   
        }
    }
}
if (isset($_POST['reserva_Socio'])){
    $ahora = new DateTime(date("Y-m-d H:i:s"));
    $hoy = date("Y-m-d");
    $hora_hoy = date("H:i:s");
    $conectando = conectar();
    $reserva_Socio = $_POST['reserva_Socio'];
    $porcentaje_plazas = $_POST['plataforma'];
    $fecha_actividad = date("Y-m-d",strtotime($_POST['fecha_actividad']));
    
    
    //variable que distingue entre reserva y confirmacion para reutilizar la funcion quedan5m para ambas.
    $tipo = 'reserva';
   
    $reserva_Actividad = $_POST['reserva_Actividad'];
    
    //comprobamos que no se pueda volver a reservar una misma actividad en un dia determinado.
    duplicidad_reserva($reserva_Socio,$reserva_Actividad);
//    $hora_actividad = date("H:i",strtotime($_POST['hora_actividad']));
   $hora_actividad = $_POST['hora_actividad'];
    //comprobamos que no tenga ya una reserva ese dia. Solo se hace en la reserva online
    // por que desde totem se pueden reservas varias al dia y en recepcion tambien.
    if ($porcentaje_plazas === 'online'){
        reserva_diaria($reserva_Socio,$fecha_actividad);
        reserva_24h($reserva_Actividad);
        reserva_hayPlazas($reserva_Actividad,$porcentaje_plazas,$tipo); 
        
    }
    if ($porcentaje_plazas === 'totem'){
//        echo("Hora ahora: ".$hora_hoy);
//        echo("Hora Actividad: ".$hora_actividad);
//        echo("Fecha ahora: ".$hoy);
//        echo("Fecha Actividad: ".$fecha_actividad);
       //reserva para el mismo dia desde totem
        if ($hoy === $fecha_actividad){
           
            if($hora_hoy > $hora_actividad){
                
//                echo($hora_hoy);
//                echo($hora_actividad);
                echo("Plazo de reserva finalizado para esta actividad.");
                exit();
            }else{
               //reserva desde totem en plazo de día. Confirmamos que esta en franja horaria y si hay plazas. 
               rango_horario($hora_hoy,$hora_actividad);
               reserva_hayPlazas($reserva_Actividad,$reserva_Socio,$porcentaje_plazas,$tipo);  
            }
        }else{
            //reserva para el dia siguiente desde totem
            if($hoy < $fecha_actividad){
               
                reserva_diaria($reserva_Socio,$fecha_actividad);
                reserva_24h($reserva_Actividad);
                reserva_hayPlazas($reserva_Actividad,$reserva_Socio,$porcentaje_plazas,$tipo);
            }else{
//               echo($hoy);
//               echo($fecha_actividad);
               echo("Plazo de reserva finalizado para esta actividad.");
               exit(); 
            }
        
        }
//       
    }
    $fecha_reserva = date("Y-m-d H:i:s"); 
    
    $sql = "INSERT INTO BPXPORT.RESERVAS(ID, ID_ACTIVIDAD, SOCIO, FECHA, CONFIRMADA) VALUES ('','$reserva_Actividad','$reserva_Socio','$fecha_reserva','0')";
    $result= mysqli_query($conectando, $sql);
    if ($result) {
        echo ("Reserva de Actividad guardada correctamente");
//        if(quedan_5m($reservas, $confirmadas, $n_plazas, $fecha_act_reserv) == true){           
//            //confirmamos la reserva automaticamente.  
//            $sql_confirmando = "UPDATE BPXPORT.RESERVAS SET CONFIRMADA ='1' WHERE ID_ACTIVIDAD = '$reserva_Actividad' AND SOCIO ='$reserva_Socio'";
//            $result_confirma= mysqli_query($conectando, $sql_confirmando);
//            if ($result_confirma) {
//                echo "Actividad Confirmada";
//            }else{
//                echo "Error: " . $sql_confirmando . "<br>" . mysqli_error($conectando);
//            }
//        }
        
    }else{
        echo "Error: " . $sql . "<br>" . mysqli_error($conectando);
    }
    
    
    
    
    mysqli_close($conectando);
   
}
if (isset($_POST['eliminar_reserva'])){
    $reserva = $_POST['eliminar_reserva'];
    $socio = $_POST['socio'];
    $conectando = conectar();
    $sql = "DELETE FROM BPXPORT.RESERVAS WHERE ID_ACTIVIDAD = '$reserva' AND SOCIO ='$socio'";
    $result= mysqli_query($conectando, $sql);
    if ($result) {
        echo "Actividad Eliminada";
    }else{
        echo "Error: " . $sql . "<br>" . mysqli_error($conectando);
    }
    
    mysqli_close($conectando);
}
if (isset($_POST['confirmar_reserva'])){
    $reserva = $_POST['confirmar_reserva'];
    $socio = $_POST['socio'];
    $conectando = conectar();
    $sql = "UPDATE BPXPORT.RESERVAS SET CONFIRMADA ='1' WHERE ID_ACTIVIDAD = '$reserva' AND SOCIO ='$socio'";
    $result= mysqli_query($conectando, $sql);
    if ($result) {
        echo "Actividad Confirmada";
    }else{
        echo "Error: " . $sql . "<br>" . mysqli_error($conectando);
    }
    
    mysqli_close($conectando);
}
if (isset($_POST['confirma_actividad_totem'])){
    $actividad_totem = $_POST['confirma_actividad_totem'];
    $socio_totem = $_POST['socio_totem'];
    $plataforma = 'totem';
    $tipo = 'confirma';
    
    reserva_hayPlazas($actividad_totem,$socio_totem,$plataforma,$tipo);
    
    $conectando = conectar();
    $sql_confirmando = "UPDATE BPXPORT.RESERVAS SET CONFIRMADA ='1' WHERE ID_ACTIVIDAD = '$actividad_totem' AND SOCIO ='$socio_totem'";
    $resultado = mysqli_query($conectando, $sql_confirmando);
    if ($resultado) {
        echo("Confirmacion realizada con éxito.");
        
    }else{
        echo "Error: " . $sql_confirmando . "<br>" . mysqli_error($conectando);
    }
    
    mysqli_close($conectando);
    
    
    
}
if (isset($_POST['reservas_Actividad'])){
    //iniciamos sesion para saber si esta logeado y si es tipo administrador o usuario
    session_start();
    $tipo_usuario= $_SESSION['tipo'];
    
    $cod_actividad = $_POST['reservas_Actividad'];
    $color = $_POST['color'];
    //Conectamos con la bd 
     
    $conectando = conectar();
    
    
    $sql ="SELECT BPXPORT.RESERVAS.ID_ACTIVIDAD, BPXPORT.RESERVAS.SOCIO, BPXPORT.RESERVAS.CONFIRMADA,
            BPXPORT.SOCIOS.CODIGO, BPXPORT.SOCIOS.NOMBRE, BPXPORT.SOCIOS.APELLIDOS
            FROM BPXPORT.RESERVAS, BPXPORT.SOCIOS 
            WHERE BPXPORT.RESERVAS.ID_ACTIVIDAD = $cod_actividad
            AND BPXPORT.RESERVAS.SOCIO = BPXPORT.SOCIOS.CODIGO";
           
    $result = mysqli_query($conectando, $sql);
    echo("<table class='table'>
          <tr><td>Codigo</td><td>Nombre</td><td>Apellidos</td></tr>");
    // comienza un bucle que leerá todos los registros existentes
    while($row = mysqli_fetch_array($result)) {
        $socio = $row['SOCIO'];
        $nombre_socio = $row['NOMBRE'];
        $apellidos_socio = $row['APELLIDOS'];
        $actividad_confirmada= $row['CONFIRMADA'];
        $tipo = '2';
        //$confirmada_sn = texto_confirmada($actividad_confirmada);
        $tot_reservas += 1;
        
        if ($actividad_confirmada == '0'){
            $clase_confirma = "rojo";
        }else {
            $clase_confirma = "verde";
            $tot_confirmadas += 1;
        }
        echo("<tr class='$clase_confirma table-hover' id='linea_reserva' style='width:200px !important'>"
             ."<td>$socio</td><td>$nombre_socio</td><td>$apellidos_socio</td>");
        if($tipo_usuario == 'Administrador'){ 
        echo("<td><span id ='icon-elim-reserva' class='fa fa-trash-alt' onclick='elimina_reserva($cod_actividad,$socio,$tipo);'></span></td><td><span id ='icon-confirm-reserva' class='fa fa-check-circle' onclick='confirma_reserva($cod_actividad,$socio,$tipo;'></span></td></tr>");    
        }
    }
        $porc_confirmadas = (($tot_confirmadas/$tot_reservas)*100)." %";
        mysqli_free_result($result); // Liberamos los registros
        mysqli_close($conectando); // Cerramos la conexion con la base de datos
        
        echo("</table>");
        echo("<div class='modal-footer'>"   
                ."<label>Total Reservas</label><input type='text' class='form-control' value ='$tot_reservas' style='width:100px'/>" 
                ."<label>Total Confirmadas</label><input type='text' class='form-control' value ='$tot_confirmadas' style='width:100px'/>"
                ."<label>% Confirmadas</label><input type='text' class='form-control' value ='$porc_confirmadas' style='width:100px'/>"
                ."</div>");                       

}

if (isset($_POST['accion_socios'])){
    $conectando = conectar();
    $accion= $_POST['accion_socios'];
    $codigo= $_POST['codigo'];
    $codigo_pul= $_POST['codigo_pul'];
    $nombre= $_POST['nombre'];
    $apellidos= $_POST['apellidos'];
    $nif= $_POST['nif'];
    $movil= $_POST['movil'];
    $tel1= $_POST['tel1'];
    $tel2= $_POST['tel2'];
    $mail= $_POST['mail'];
    $baja=$_POST['baja'];
    
    switch($accion){
        case 'agregar':
            //comprobamos que no exista ya este codigo en la base de datos
            $consulta_existe="SELECT * FROM BPXPORT.SOCIOS WHERE CODIGO = '$codigo'";
            $resultado = mysqli_query($conectando,$consulta_existe) or die (mysqli_error());
            if (mysqli_num_rows($resultado)>0){
                echo("Este código de asociado ya existe. Posible duplicidad de código.");
                exit();
            }    
            
            $sql = "INSERT INTO BPXPORT.SOCIOS SET 
                    CODIGO = '$codigo',
                    CODIGO_PUL = '$codigo_pul',
                    NOMBRE = '$nombre',
                    APELLIDOS = '$apellidos',
                    NIF = '$nif',
                    MOVIL = '$movil',
                    TEL1 = '$tel1',
                    TEL2 = '$tel2',
                    EMAIL = '$mail',
                    FECHA_BAJA = '$baja'";
            $result = mysqli_query($conectando, $sql);
            if ($result) {
                echo "Nuevo socio Insertado con éxito.";
            } else {
                echo "Error: " . $sql . "<br>" . $conectando->error;
            }   

           mysqli_close($conectando);
            break;
        case 'eliminar':

            if(isset($codigo)){
                    $sql = "DELETE FROM BPXPORT.SOCIOS WHERE CODIGO = '$codigo'";
                    $result = mysqli_query($conectando, $sql);
                     if ($result) {
                            echo "Ficha de Socio eliminada con éxito.";
                    } else {
                            echo "Error: " . $sql . "<br>" . $conectando->error;
                    }   
            }
             mysqli_close($conectando);
         break;
        case 'modificar':
           
            $sql = "UPDATE BPXPORT.SOCIOS SET
                                    CODIGO = '$codigo',
                                    CODIGO_PUL = '$codigo_pul',
                                    NOMBRE = '$nombre',
                                    APELLIDOS = '$apellidos',
                                    NIF = '$nif',
                                    MOVIL = '$movil',
                                    TEL1 = '$tel1',
                                    TEL2 = '$tel2',
                                    EMAIL = '$mail',
                                    FECHA_BAJA = '$baja'  
                                    WHERE CODIGO = '$codigo'";
            $result = mysqli_query($conectando, $sql);
            if ($result) {
                echo "Ficha de Socio modificada con éxito.";
            } else {
                echo "Error: " . $sql . "<br>" . $conectando->error;
            }   

           mysqli_close($conectando);
         break;   
    }
}
if (isset($_POST['accion_usuarios'])){
    $conectando = conectar();
    $accion= $_POST['accion_usuarios'];
    $nombre= $_POST['nombre'];
    $contra= $_POST['contra'];
    $tipo= $_POST['tipo'];
    $id = $_POST['id'];
    
    switch($accion){
        case 'agregar':
            $sql = "INSERT INTO BPXPORT.USUARIOS SET 
                    NOMBRE = '$nombre',
                    CONTRA = '$contra',
                    TIPO = '$tipo'";
            $result = mysqli_query($conectando, $sql);
            if ($result) {
                echo "Nuevo Usuario Insertado con éxito.";
            } else {
                echo "Error: " . $sql . "<br>" . $conectando->error;
            }   

           mysqli_close($conectando);
            break;
        case 'eliminar':

            if(isset($nombre)){
                    $sql = "DELETE FROM BPXPORT.USUARIOS WHERE ID = '$id'";
                    $result = mysqli_query($conectando, $sql);
                     if ($result) {
                            echo "Ficha de Usuario eliminada con éxito.";
                    } else {
                            echo "Error: " . $sql . "<br>" . $conectando->error;
                    }   
            }
             mysqli_close($conectando);
         break;
        case 'modificar':
           
            $sql = "UPDATE BPXPORT.USUARIOS SET
                                    NOMBRE = '$nombre',
                                    CONTRA = '$contra',
                                    TIPO = '$tipo'
                                    WHERE ID = '$id'";
            $result = mysqli_query($conectando, $sql);
            if ($result) {
                echo "Ficha de Usuario modificada con éxito.";
            } else {
                echo "Error: " . $sql . "<br>" . $conectando->error;
            }   

           mysqli_close($conectando);
         break;   
    }
}

if (isset($_POST['historial_reservas_socio'])){
    //iniciamos sesion para saber si esta logeado y si es tipo administrador o usuario
    
    session_start();
    $tipo_usuario= $_SESSION['tipo'];
    
    $cod_socio_historial = $_POST['historial_reservas_socio'];
    
    //Conectamos con la bd 
     
    $conectando = conectar();
    
    
    $sql ="SELECT BPXPORT.RESERVAS.ID_ACTIVIDAD, BPXPORT.RESERVAS.SOCIO, BPXPORT.RESERVAS.CONFIRMADA,BPXPORT.RESERVAS.CONFIRMADA,BPXPORT.RESERVAS.FECHA,
            BPXPORT.ACTIVIDADES.id, BPXPORT.ACTIVIDADES.title
            FROM BPXPORT.RESERVAS, BPXPORT.ACTIVIDADES 
            WHERE BPXPORT.RESERVAS.SOCIO = $cod_socio_historial
            AND BPXPORT.ACTIVIDADES.id = BPXPORT.RESERVAS.ID_ACTIVIDAD";
           
    $result = mysqli_query($conectando, $sql);
    //echo("<span id='cierra_historial_socio' class='fas fa-times-circle' onclick='cierra_historial_socio()'></span><br>");
    echo("<table class='table' style='table-layout: fixed;overflow-x:auto;max-height:50px;width:90%;'>
          <tr  style='overflow-x:auto' id='cab_historial_reservas'><td>Actividad</td><td>Fecha</td><td>Confirmada</td></tr>");
    // comienza un bucle que leerá todos los registros existentes
    while($row = mysqli_fetch_array($result)) {
        $cod_actividad_historial = $row['ID_ACTIVIDAD'];
        $socio = $row['SOCIO'];
        $actividad = $row['title'];
        $fecha = $row['FECHA'];
        $actividad_confirmada= $row['CONFIRMADA'];
        $tipo = '3';
        //$confirmada_sn = texto_confirmada($actividad_confirmada);
        $tot_reservas += 1;
        $actualizar = "1";
        
        if ($actividad_confirmada == '0'){
            $clase_confirma = "rojo";
            $texto_confirmada= "No";
        }else {         
            $clase_confirma = "verde";
            $texto_confirmada= "Sí";
            $tot_confirmadas += 1;
        }
        echo("<tr class='$clase_confirma' table-hover' id='linea_reserva' style='width:200px !important'>"
             ."<td>$actividad</td><td>$fecha</td><td style='text-align:center;'>$texto_confirmada</td>");
        
        if($tipo_usuario == 'Administrador'){ 
        echo("<td><span id ='icon-elim-reserva' class='fa fa-trash-alt'  style='float:right;padding-right:35px;width:5px;' onclick='elimina_reserva($cod_actividad_historial,$socio,$tipo),mostrar_historial_socio($actualizar);'></span></td>");
            if($clase_confirma == 'rojo'){
                 echo("<td style='width:5px;'><span id ='icon-confirm-reserva' class='fa fa-check-circle'  style='float:left;' onclick='confirma_reserva($cod_actividad_historial,$socio,$tipo),mostrar_historial_socio($actualizar);'></span></td></tr>");    
            }        
        }
       
    }
        echo("</table>");
        $porc_confirmadas = (($tot_confirmadas/$tot_reservas)*100)." %";
        mysqli_free_result($result); // Liberamos los registros
        mysqli_close($conectando); // Cerramos la conexion con la base de datos
        

        echo("<div class='form-row'>"   
                ."<div class='col-sm-2'><label class='pie_historial'>Reservas</label><input type='text' class='form-control' value ='$tot_reservas' style='text-align:center;' /></div>" 
                ."<div class='form-group col-sm-2'><label class='pie_historial'>Confirmadas</label><input type='text' class='form-control' value ='$tot_confirmadas' style='text-align:center;'/></div>"
                ."<div class='form-group col-sm-2'><label class='pie_historial'>%</label><input type='text' class='form-control' value ='$porc_confirmadas' style='text-align:center;' /></div><div class='form-group col-sm-6'></div>"
                ."</div>");                       

}
if (isset($_POST['galeria_actividades'])){
    $directory="imagenes/galeria_actividades";
    $dirint = dir($directory);
    while (($archivo = $dirint->read()) !== false)
    {
        if (eregi("gif", $archivo) || eregi("jpg", $archivo) || eregi("png", $archivo)){
            echo '<img src="'.$directory."/".$archivo.'" style="width:200px;height:150px">'."\n";
        }
    }
    $dirint->close();
}

if(isset($_POST['clave_aleatoria'])){
    $mail_codigo_seguridad = $_POST['clave_aleatoria'];
    $nif_nuevo_user = $_POST['nif'];
   
    $longitud_codigo = '6';
    $codigo_aleatorio = generar_codigo_aleatorio($longitud_codigo);
    envio_mail_verifica_nuevo_usuario($mail_codigo_seguridad, $codigo_aleatorio,$nif_nuevo_user);
   

}
if(isset($_POST['clave_verifi'])){
    session_start();
    $codigo_enviado = $_SESSION['codigo_verifi'];
    $nif_enviado = $_SESSION['nif_user'];
    
    $codigo_int = $_POST['clave_verifi'];
    $nif_int = $_POST['nif'];
    $nombre_int =  $_POST['nombre'];
    $apel_int =  $_POST['apel'];
    $movil_int =  $_POST['movil'];
    $tel_int =  $_POST['tel'];
    $mail_int =  $_POST['mail'];
    
    if ($codigo_enviado === $codigo_int && $nif_enviado === $nif_int){
        $conectando = conectar();
       //comprobamos que no exista ya el usuario antes de insertar
        
        $consulta_existe="SELECT * FROM BPXPORT.SOCIOS WHERE NIF = '$nif_enviado'";
        $resultado = mysqli_query($conectando,$consulta_existe) or die (mysqli_error());
        if (mysqli_num_rows($resultado)>0){
            echo("El usuario que quiere darse de alta ya existe. Posible duplicidad de datos.");
            session_destroy();
            exit();
        }    
        $sql = "INSERT INTO 
                BPXPORT.SOCIOS(ID,CODIGO,CODIGO_PUL,NOMBRE,APELLIDOS,NIF,MOVIL,TEL1,TEL2,EMAIL,FECHA_BAJA)
                VALUES('','','','$nombre_int','$apel_int','$nif_int','$movil_int','$tel_int','','$mail_int','')";
        $result= mysqli_query($conectando, $sql);
        if ($result) {
            echo ("Usuario registrado correctamente");
            session_destroy();

        }else{
            echo "Error: " . $sql . "<br>" . mysqli_error($conectando);
        }
    }else{
        echo("Codigo de verificación incorrecto");
        
    }
    
}


   if(isset($_FILES['file'])){
       
  
    if ($_FILES['file']["error"] > 0) {
        echo "Error: " . $_FILES['file']['error'] . "<br>";

    }else{
//        echo "Nombre: " . $_FILES['fichero_origen']['name'] . "<br>";
//        echo "Tipo: " . $_FILES['fichero_origen']['type'] . "<br>";
//        echo "Tamaño: " . ($_FILES["fichero_origen"]["size"] / 1024) . " kB<br>";
//        echo "Carpeta temporal: " . $_FILES['fichero_origen']['tmp_name'];

        move_uploaded_file($_FILES['file']['tmp_name'],"/var/www/html/Bpxport/archivos/subidas/" . $_FILES['file']['name']);
        
        echo ("Fichero subido correctamente");
    }

}

 function reserva_diaria($_Socio,$fecha_actividad){
    
//   echo($fecha_actividad);
//   exit();
    $conectando = conectar();
    
    //$sql = "SELECT SOCIO,start FROM BPXPORT.RESERVAS, BPXPORT.ACTIVIDADES where SOCIO = '$_Socio' AND ID_ACTIVIDAD = id AND DATE_FORMAT(start,'%Y-%m-%d') = '$fecha_actividad'";
    
    
    $sql ="SELECT BPXPORT.RESERVAS.ID_ACTIVIDAD, BPXPORT.RESERVAS.SOCIO, 
            BPXPORT.ACTIVIDADES.id, BPXPORT.ACTIVIDADES.start 
            FROM BPXPORT.RESERVAS, BPXPORT.ACTIVIDADES 
            WHERE BPXPORT.RESERVAS.ID_ACTIVIDAD = BPXPORT.ACTIVIDADES.id
            AND BPXPORT.RESERVAS.SOCIO = '$_Socio' AND DATE_FORMAT(BPXPORT.ACTIVIDADES.start, '%Y-%m-%d') = '$fecha_actividad'";
    $resul = mysqli_query($conectando, $sql);
    
    $fila = mysqli_fetch_row($resul);
    
    if(mysqli_num_rows($resul) > 0){
        echo("Ya tiene una reserva para este dia.");
        exit();
    }
 }    
 function reserva_24h($_actividad){
    $ahora = new DateTime(date("Y-m-d H:i:s"));
    //echo(date("Y-m-d h:i:00"));
    $conectando = conectar();
    $sql = "SELECT id,start FROM BPXPORT.ACTIVIDADES where id = '$_actividad'";
    $resul = mysqli_query($conectando, $sql);
    $fila = mysqli_fetch_row($resul);
    if(mysqli_num_rows($resul) > 0){
        $fecha_actividad = new DateTime($fila[1]); 
        $intervalo = $fecha_actividad->diff($ahora);
      
        $diff_d = $intervalo->format('%a'); // dias de dif.
        $diff_h = $intervalo->format('%h'); // horas de dif.
        $diff_m = $intervalo->format('%i'); // minutos ...
        $diff_s = $intervalo->format('%s'); // segundos ..
//        echo("fecha actividad: ".$fila[1]."---");
//        echo ("dias: ".$diff_d."---");
//        echo ("horas: ".$diff_h."---");
//        echo ("minutos: ".$diff_m."---");
//        echo ("Segundos: ".$diff_s);
       
        if (($diff_d>1)or($diff_d==1 and ($diff_h >=1 or $diff_m>=1 or $diff_s >=1))or($diff_h == 24 and $diff_m > 0) or($diff_h > 24) or ($diff_h < 24 and $fecha_actividad < $ahora)) {
            echo("Reserva fuera de plazo. La reserva solo se podra realizar 24h antes de la actividad.");
            exit();    
        }
    }    
 }
function reserva_hayPlazas($_actividad,$socio,$porc_plazas,$tipo){
     
    $ahora =  date("Y-m-d");
    $conectando = conectar();
    
    //seleccionamos las plazas y fecha   para esa actividad
    $sql = "SELECT plazas,start FROM BPXPORT.ACTIVIDADES where id ='$_actividad'";
    $resul_plazas_fecha = mysqli_query($conectando, $sql);
    $registro = mysqli_fetch_row($resul_plazas_fecha);
    $plazas = $registro[0];
    $fecha_acti = $registro[1];//esta fecha es para calcular quedan5m()
    $fecha_actividad = date("Y-m-d", strtotime($registro[1]));
  
    //comprobamos si la fecha actual en la que se realiza la reserva
    //a traves del totem es la misma que la de la actividad a reservar. Es decir
    //que la reserva es en el dia.
  
    
    if ($porc_plazas === 'totem' && $fecha_actividad == $ahora){//reserva en el dia
        $limite_reservas = $plazas;
    }    
    if ($porc_plazas === 'totem' && $fecha_actividad != $ahora){//reserva para dia siguiente
         $limite = $plazas * 0.75; 
         $limite_reservas = floor($limite);
    }
    
     if ($porc_plazas === 'online'){
         $limite = $plazas * 0.75;
        $limite_reservas = floor($limite);
        
    }
     if ($porc_plazas === 'recepcion'){
        $limite_reservas = $plazas;
    }
    
    //seleccionamos el numero de reservas existentes para esa actividad
    $sql = "SELECT count(*) FROM BPXPORT.RESERVAS where ID_ACTIVIDAD = '$_actividad'";
    $resul = mysqli_query($conectando, $sql);
    $contador_reservas = mysqli_fetch_row($resul);
    $reservas =  $contador_reservas[0];
    //seleccionamos el numero de reservas confirmadas para esa actividad
    //para saber si se pueden liberar reservas al no estar confirmadas y quedar < 5min.
    $sql_confirmadas = "SELECT count(*) FROM BPXPORT.RESERVAS where ID_ACTIVIDAD = '$_actividad' AND CONFIRMADA = '1'";
    $resul_confirmadas = mysqli_query($conectando, $sql_confirmadas);
    $contador_confirmadas = mysqli_fetch_row($resul_confirmadas);
    $reservas_confirmadas =  $contador_confirmadas[0];
//    echo("las reservas actuales son de : ".$reservas);
//    echo("el limite de rerservas es de :".$limite_reservas);
    if ($tipo === 'reserva'){
        if($limite_reservas === $reservas){

            //si se reserva desde el totem para ese dia y no hay plazas
            // preguntamos si quedan 5 min. para la actividad. Si no estan todas las
            //  plazas reservadas confirmadas se liberan y se deja confirmar.
            if ($porc_plazas === 'totem' && $fecha_actividad === $ahora){

               quedan_5m($reservas,$reservas_confirmadas,$limite_reservas,$fecha_acti,$tipo);

            }else{
                 echo("Plazas agotadas para esta actividad. Consulte en recepción.");                
                 exit();
            }
        }
    }else{
        quedan_5m($reservas,$reservas_confirmadas,$limite_reservas,$fecha_acti,$tipo);
    }   
}   

 
function quedan_5m($reservas,$confirmadas,$n_plazas,$fecha_act_reserv,$tipo){ 
   $ahora = new DateTime(date('Y-m-d H:i:s'));
   $fecha_act_reserva_datetime = new DateTime($fecha_act_reserv);
     
    $intervalo = $ahora->diff($fecha_act_reserva_datetime);
    
 
    $diff_d = $intervalo->format('%a'); // dias de dif.
    $diff_h = $intervalo->format('%h'); // horas de dif.
    $diff_m = $intervalo->format('%i'); // minutos ...
    $diff_s = $intervalo->format('%s'); // segundos ..
   //calculamos la diferencia en horas para saber si puede confirmar en ese momento una actividad

    if ($diff_d === '0' && $diff_h === '0' && $diff_m <= '5' && $diff_s <= '60' ) {
        if ($tipo === 'reserva'){
            if ($reservas === $confirmadas && $confirmadas === $n_plazas){
                echo("Imposible reservar. Todas las plazas están confirmadas.");
                exit();              
            }
        }else{
            if ($reservas >= $confirmadas && $confirmadas === $n_plazas){
                echo("Imposible confirmar. Todas las plazas están confirmadas.");
                exit();              
            }
        }
       
    }else{
        if ($tipo = 'reserva'){
            if ($reservas === $n_plazas){
                echo("Imposible reservar. Todas las plazas están reservadas. Espera hasta 5 min. antes de la actividad o consulte en recepción.");
                exit();
            }
        }
    }
    
    
}
function rango_horario($hora,$hora_activi) {
   
    
   if(($hora >= '07:30:00' and $hora <= '15:00:00') AND ($hora_activi >= '07:30:00' and $hora_activi <= '15:00:00')) { 
     
       }elseif ($hora_activi >= '07:30:00' and $hora_activi <= '15:00:00'){
            echo("la franja horaria no es correcta");
            exit();
    }
    if(($hora >= '15:00:00' and $hora <= '22:00:00') AND ($hora_activi >= '15:00:00' and $hora_activi <= '22:00:00')){
      
        }elseif ($hora_activi >= '15:00:00' and $hora_activi <= '22:00:00'){
          echo("la franja horaria no es correcta");
          exit();   
    }
   
}
function duplicidad_reserva($socio_dupli,$actividad_dupli){
    $conectando = conectar();
    $sql = "SELECT count(*) FROM BPXPORT.RESERVAS where SOCIO = '$socio_dupli' AND ID_ACTIVIDAD = '$actividad_dupli'";
    $resul = mysqli_query($conectando, $sql);
    $contador_duplicados = mysqli_fetch_row($resul);
    if($contador_duplicados[0] > 0){
        echo("Imposible la reserva. Esta actividad ya fué reservada en la fecha indicada.");
        exit();
    }
}


 ?>
