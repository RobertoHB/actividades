<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="estilos/estilos.css">
    <link rel="stylesheet" href="css/fontawesome-all.min.css">
     <!--librerias bootstrap-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="librerias/javascript/jquery.min.js"></script>
    <script src="librerias/javascript/moment.min.js"></script>
    
   
    <!--libreria js-->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
   
    <script language="JavaScript" SRC="funciones.js"></script>
   <!--ClockPickerr-->
   <link rel="stylesheet" href="librerias/css/bootstrap-clockpicker.css">
   <script src="librerias/javascript/bootstrap-clockpicker.js"></script>  


  
  <title>Comunicación Socios</title>
</head>
<body>
    <div class="plantilla_correo">
        <span class="fa fa-paper-plane" id="ico_envio_mail" onclick="enviar_mail()">Enviar</span><div id="aviso"></div><hr>
        <div class="mail-para">Para</div><textarea id="dir-correo"></textarea><br>
        <div class="check_abonado"><input type="radio" id="check_abo" name="check_abonados" value="Abonados" checked onclick="relleno_para_mails(value)">Abonados<br>
            <input type="radio" id="check_noabo" name="check_abonados" value="No Abonados"  onclick="relleno_para_mails(value)">No Abonados
        </div><hr>
        <input id="asunto" placeholder="Agregar un asunto"/><hr>
        <textarea id="mensaje"></textarea>
        
    </div>
    
   
</body>
<script>
function enviar_mail(){
    var para = $('#dir-correo').val();
    var asunto = $('#asunto').val();
    var mensaje = $('#mensaje').val();
    $('#aviso').html('<div class="loading"><img src="imagenes/35.gif" alt="loading" /><br/>Enviando...</div>');
    $.post('sms_socios.php',{para:para,asunto:asunto,mensaje:mensaje}, function(resultado){
        $('#aviso').fadeTo(100, 0.1).fadeTo(200, 1.0).html(resultado);
       
    });

}

function relleno_para_mails(valor){
var valor = valor;
  $.post('sms_socios.php',{lista_mails:valor}, function(resultado){
    $('#dir-correo').val(resultado);   
//    $.post('sms_socios.php',{insertar_img:1}, function(resultado){
//      $('#mensaje').html(resultado);
//    });
  });  
}
</script>    

</html>