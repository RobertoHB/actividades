<?php
include("funciones.php");
include("cerrar_sesion_totem.php");
//calculamos los caracteres aleatorios por si se identifica por el NIF
$longitud_codigo = "3";
$codigo_abc = generar_codigo_abc($longitud_codigo);
$letra_nif_1 = substr($codigo_abc, 0, 1); 
$letra_nif_2 = substr($codigo_abc, 1, 1); 
$letra_nif_3 = substr($codigo_abc, 2, 2); 

?>
<html>
<head>
  <meta charset="UTF-8">
    <meta name="viewpoort" content="width=device-width,user-scalable=no, initial-scale=1.0,
          maximum-scale=1.0, minimum-scale=1.0">
    <!--librerias jquery-->
     <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="librerias/javascript/jquery.easy-autocomplete.min.js"></script> 
     <!--css que contiene las CSS del autocomplete-->
     <link rel="stylesheet" href="librerias/css/easy-autocomplete.css">
     <link rel="stylesheet" href="librerias/css/easy-autocomplete.themes.css">
     
   <!--librerias bootstrap-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jqbtk/jqbtk.min.css">
    
    <!--<script type="text/javascript" src="librerias/javascript/jquery.js"></script>-->
     <link rel="stylesheet" href="estilos/estilos.css">
     
     <!--css que contiene las fuentes e iconos de la app-->
     <link rel="stylesheet" href="css/fontawesome-all.min.css">
     
    <script src="librerias/javascript/moment.min.js"></script>

    <!--full-calendar-->
    <link rel="stylesheet" href="librerias/css/fullcalendar.min.css">
    <script src="librerias/javascript/fullcalendar.min.js"></script>  
    <script src="librerias/javascript/es.js"></script> 
    <!--libreria js-->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="librerias/javascript/jqbtk.min.js"></script>
    
        <!--autocomplete-->
   

    <script language="JavaScript" SRC="funciones.js"></script>
   <!--ClockPickerr-->
   <link rel="stylesheet" href="librerias/css/bootstrap-clockpicker.css">
   <script src="librerias/javascript/bootstrap-clockpicker.js"></script>  
    
</head>  


<body>   
    
<header>
    <div class="container-menu">                               
            <div class="logo">
                <div class="logo-name col-sm-6">
                    <img src="imagenes/logo_bpx.png" style="width: 65%;margin-left:40px;"/>
                </div>   
<!--                <div class="logo-name col-sm-5">
                    <p style="color:white;font-weight: bold;font-family: fantasy; padding-top:30px;font-size:25px">ACTIVIDADES</p>
                </div>   -->
                  <div class="logo-name col-sm-6">   
                      <a href="confirma_reserva.php" id="btn_cerrar_sesion_totem" class="btn_cerrar_sesion_totem"><i class="fa fa-home fa-5x" style="color:white !important;margin-left:300px;margin-top:8px;"></i></a>
                  </div>      
            </div>
                                   
            </div>
        </div>
</header>
<!--    <div>cabecera logo bpx + cerrar sesion</div>-->
 
    <div id="confirma_reserva" style="background-color: #C2CCD1;
                                        background: linear-gradient(#444,#C2CCD1);">
      <div class="form-group marco_confirma_reserva">
          <label>Identificación</label> <br>
          <input id="codigo_pulsera" name ="codigo_pulsera" class="form-control" placeholder="Acerque la pulsera al lector" type="text" style="text-align: center;font-size:60px" value="" onchange= "autentificar_totem('lector')" autofocus/>
          <button type="button" class="btn btn-info" id="btn_identificar" style="margin-top:240px;width:750px !important;height:80px;margin-left:20px;background-color:#A2C02B !important;font-size:50px;" onclick="autentificar_totem('teclado')">Validar</button>   
          
      </div>
    
    <div class="container" id="letras_nif" style="float:left;clear:both;margin:670 0 0 70;width:820;font-size:65px;display:none;">
        <div id="letra_nif_1" class="col-sm-3 teclado"><?php echo $letra_nif_1;?></div> 
        <div id="letra_nif_2" class="col-sm-3 teclado"><?php echo $letra_nif_2;?></div>
        <div id="letra_nif_3" class="col-sm-3 teclado"><?php echo $letra_nif_3;?></div>
        <div id="mi_letra_nif" class="col-sm-3 teclado"></div>
    </div>   
    <div class="container" id= "teclado_numerico" style="float:left;clear:both;margin:800 0 0 85;width:789px;font-size:65px;">         
        <div class="row">
            <div id="teclado_num" class="col-sm-4 teclado">7</div>
            <div id="teclado_num" class="col-sm-4 teclado">8</div>
            <div id="teclado_num" class="col-sm-4 teclado">9</div>
        </div>   
        <div class="row">   
            <div id="teclado_num" class="col-sm-4 teclado">4</div>
            <div id="teclado_num" class="col-sm-4 teclado">5</div>
            <div id="teclado_num" class="col-sm-4 teclado">6</div>    
        </div> 
        <div class="row">
            <div id="teclado_num" class="col-sm-4 teclado">1</div>
            <div id="teclado_num" class="col-sm-4 teclado">2</div>
            <div id="teclado_num" class="col-sm-4 teclado">3</div>
        </div>   
         <div class="row">
            <div id="teclado_num" class="col-sm-4 teclado">0</div>
            <div id="bot_borrar" class="col-sm-4 teclado_borrar"><span class="fa fa-arrow-left" style="line-height: 1.5 !important"></span></div>
            <div id="bot_limpiar" class="col-sm-4 teclado_limpiar">C</div>  
    
        </div>
    </div>

<script>
    $(document).ready(function(){
        $(".teclado").click(function(){
            var htmlString = $( this ).html();
            var input_actual = $("#codigo_pulsera").val();
            var concatenado = input_actual + htmlString;
            $("#codigo_pulsera").val(concatenado);   
            if(concatenado.length == '8'){
                var resto = concatenado % 23;
                calular_letra_nif(resto);
                $("#letras_nif").show();
                $("#teclado_numerico").css('margin','-640 0 0 85');
                $("#mi_letra_nif").css('background-color','white');
                tecla_nif_parpadea();
            }    
         });
        $(".teclado_limpiar").click(function(){
           $("#codigo_pulsera").blur();
           $("#codigo_pulsera").val('');
          
        });
         $(".teclado_borrar").click(function(){
             
            var cadena_actual = $("#codigo_pulsera").val();
  
            var cadena_borrada = cadena_actual.slice(0,-1);
            
            $("#codigo_pulsera").val(cadena_borrada);     
         });
        setInterval(tecla_nif_parpadea, 700); 
    });
    function tecla_nif_parpadea(){
        $("#mi_letra_nif").fadeTo(100, 0.1).fadeTo(200, 1.0);
    };
    
    function calular_letra_nif(valor){
       
        switch (valor) {
            case 0:
                $("#mi_letra_nif").html('T');
            break;
            case 1:
                $("#mi_letra_nif").html('R');
            break;
            case 2:
                $("#mi_letra_nif").html('W');
            break;
            case 3:
                $("#mi_letra_nif").html('A');
            break;
            case 4:
                $("#mi_letra_nif").html('G');
            break;
            case 5:
                $("#mi_letra_nif").html('M');
            break;
            case 6:
                $("#mi_letra_nif").html('Y');
            break;
            case 7:
                
                $("#mi_letra_nif").html('F');
            break;
            case 8:
                $("#mi_letra_nif").html('P');
            break;
            case 9:
                $("#mi_letra_nif").html('D');
            break;
            case 10:
                $("#mi_letra_nif").html('X');
            break;
            case 11:
                $("#mi_letra_nif").html('B');
            break;
            case 12:
                $("#mi_letra_nif").html('N');
            break;
              
            case 13:
                $("#mi_letra_nif").html('J');
            break;
            case 14:
                $("#mi_letra_nif").html('Z');
            break;
            case 15:
                $("#mi_letra_nif").html('S');
            break;
            case 16:
                $("#mi_letra_nif").html('Q');
            break;
            case 17:
                $("#mi_letra_nif").html('V');
            break;
            case 18:
                $("#mi_letra_nif").html('H');
            break;
            case 19:
                $("#mi_letra_nif").html('L');
            break;
            case 20:
                $("#mi_letra_nif").html('C');
            break;
            case 21:
                $("#mi_letra_nif").html('K');
            break;
            case 22:
                $("#mi_letra_nif").html('E');
            break;
              
            default:
                $("#mi_letra_nif").val('');
            break;
        }    
    }
    function autentificar_totem(valor){
    var cod_pulsera = $("#codigo_pulsera").val();
    var codigo_alernativo = $("#codigo_alterna").val();
    var tipo_identificacion = valor;
   

        if (cod_pulsera != "") {
            $.post('ajax.php', {valorpulsera: cod_pulsera,valoralternativo: codigo_alernativo,tipo:tipo_identificacion}, function(mensaje) {  
              //Utilizamos substring para evitar los espacios en blanco recibidos del mensaje
             
              var resultado = mensaje.substring(4, 5);
                
                if (resultado == 1){
                   //alert("Abonado Autentificado");
//                   $('#confirma_reserva').load('confirma_reserva_ico.php'); 
                   location.href = "confirma_reserva_ico.php";
//                   //$("#form_login").hide();
//                    
//                   //window.open('admin_actividad.php','','width=1200,height=800,location=no,statusbar=no,toolbar=no,scrollbars=no,navbar=no,menubar=no,resizable=no');
//                    //$('#myModal').modal('toggle');
//                    //$('#Modal_reservas').modal('show');
//                    //$('#myModal').modal('hide');
                }else{
                    
                    alert("Codigo no registrado en nuestra App.");
                     $("#codigo_pulsera").focusin();
                     $("#codigo_pulsera").val('');
                    //abrimos modal de introduccion codigo socio,nif ó tfno
                    
                }    
            }); 
        }else { 
            alert("Introduzca un Abonado");
        }
}


</script>
      
</body>



</html>

