<?php
include("funciones.php");
include("conexion.php");
ini_set("session.gc_maxlifetime", 60);

?>
<html>
<head>
  <meta charset="UTF-8">
    <meta name="viewpoort" content="width=device-width,user-scalable=no, initial-scale=1.0,
          maximum-scale=1.0, minimum-scale=1.0">
    <!--librerias jquery-->
     <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="librerias/javascript/jquery.easy-autocomplete.min.js"></script> 
     <!--css que contiene las CSS del autocomplete-->
     <link rel="stylesheet" href="librerias/css/easy-autocomplete.css">
     <link rel="stylesheet" href="librerias/css/easy-autocomplete.themes.css">
     
   <!--librerias bootstrap-->
   <link rel="stylesheet" href="estilos/estilos.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    
    
    <!--<script type="text/javascript" src="librerias/javascript/jquery.js"></script>-->
     <link rel="stylesheet" href="estilos/estilos.css">
     
     <!--css que contiene las fuentes e iconos de la app-->
     <link rel="stylesheet" href="css/fontawesome-all.min.css">
     
    <script src="librerias/javascript/moment.min.js"></script>

    <!--full-calendar-->
    <link rel="stylesheet" href="librerias/css/fullcalendar.min.css">
    <script src="librerias/javascript/fullcalendar.min.js"></script>  
    <script src="librerias/javascript/es.js"></script> 
    <!--libreria js-->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    
        <!--autocomplete-->
   

    <script language="JavaScript" SRC="funciones.js"></script>
   <!--ClockPickerr-->
   <link rel="stylesheet" href="librerias/css/bootstrap-clockpicker.css">
   <script src="librerias/javascript/bootstrap-clockpicker.js"></script>  
    
</head>  


<body>   
    
<div class="menu_login_confirma_reserva">                               
    <div class="row logo_confirma_reserva">
    <?php
    session_start();
    $nombre_abonado = $_SESSION['NOMBRE'];
    $apellidos_abonado = $_SESSION['APELLIDOS'];
    $codigo_abonado = $_SESSION['CODIGO'];
    ?>
        <div style="padding-top:100px;padding-left:50px;float:left;">
        <label>Abonado:  <?php echo($nombre_abonado." ". $apellidos_abonado. "(". $codigo_abonado. ")");?></label><br>
        <!--<label>Nombre/Apellidos: <?php echo($nombre_abonado." ". $apellidos_abonado);?></label><br>-->
        </div>
        <div class="logo1_confirma_reserva">
           
            <img src="imagenes/logo1.png" onclick="reserva_totem()">
            <div class="centrado">Reservar</div>
        </div>
        <div class="logo2_confirma_reserva">        
            <img  src="imagenes/logo4.png" onclick="confirma_totem()">
            <div class="centrado">Confirmar</div>          
        </div>
    </div><hr>
</div>
<div id='ventanas_confirma_reserva' style="max-width: 800px;"></div>


 
    
              
</body>
<script>
function confirma_totem(){
     $('#ventanas_confirma_reserva').html('');
    $('#ventanas_confirma_reserva').load('mis_reservas.php'); 
}    
function reserva_totem(){
    $('#ventanas_confirma_reserva').html('');
    $('#ventanas_confirma_reserva').fullCalendar({
    height: 200,
    width: 200,
    defaultView: 'listWeek',
    editable:true,

    header: { 
        left: '',   
        center: 'title',
//        right: 'listDay',
    },
    views: { // set the view button names
        listWeek: {buttonText: 'Diario'}

    },
   
    events:'actividades.php',

    eventClick:function(calEvent,jsEvent,view){                   

        $('#txtID').val(calEvent.id);
        $('#txtActividad').val(calEvent.title);
        $('#txtMoniSala').val(calEvent.descripcion);

        $('#txtColor').val(calEvent.color); 
         //En la variable FechaHora se divide con split porr un lado la fecha [0](recoge hasta el primer espacio) y por otro la hora [1](recoge despues del espacio)
        FechaHora = calEvent.start._i.split(" ");


        $('#txtFecha').val(FechaHora[0]);
        $('#txtHora').val(FechaHora[1]);

        $("#Modal_reservas").modal();

    }
 
      
      
   });
    

}
</script>    


</html>

