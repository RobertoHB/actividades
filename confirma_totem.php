<?php
include("funciones.php");
include("conexion.php");
ini_set("session.gc_maxlifetime", 60);

?>
<html>
<head>
  <meta charset="UTF-8">
    <meta name="viewpoort" content="width=device-width,user-scalable=no, initial-scale=1.0,
          maximum-scale=1.0, minimum-scale=1.0">
    <!--librerias jquery-->
     <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="librerias/javascript/jquery.easy-autocomplete.min.js"></script> 
     <!--css que contiene las CSS del autocomplete-->
     <link rel="stylesheet" href="librerias/css/easy-autocomplete.css">
     <link rel="stylesheet" href="librerias/css/easy-autocomplete.themes.css">
     
   <!--librerias bootstrap-->
   <link rel="stylesheet" href="estilos/estilos.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    
    
    <!--<script type="text/javascript" src="librerias/javascript/jquery.js"></script>-->
     <link rel="stylesheet" href="estilos/estilos.css">
     
     <!--css que contiene las fuentes e iconos de la app-->
     <link rel="stylesheet" href="css/fontawesome-all.min.css">
     
    <script src="librerias/javascript/moment.min.js"></script>

    <!--full-calendar-->
    <link rel="stylesheet" href="librerias/css/fullcalendar.min.css">
    <script src="librerias/javascript/fullcalendar.min.js"></script>  
    <script src="librerias/javascript/es.js"></script> 
    <!--libreria js-->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    
        <!--autocomplete-->
   

    <script language="JavaScript" SRC="funciones.js"></script>
   <!--ClockPickerr-->
   <link rel="stylesheet" href="librerias/css/bootstrap-clockpicker.css">
   <script src="librerias/javascript/bootstrap-clockpicker.js"></script>  
    
</head>  


<body>   
<header>
    <div class="container-menu">                               
            <div class="logo">
                <div class="logo-name col-sm-6">
                    <img src="imagenes/logo_bpx.png" alt="" style="width: 65%;margin-left:40px;">
                </div>   
<!--                <div class="logo-name col-sm-5">
                    <p style="color:white;font-weight: bold;font-family: fantasy; padding-top:25px;font-size:16px">Confirmación de Actividades</p>
                </div>   -->
                  <div class="logo-name col-sm-6">   
                      <a href="confirma_reserva_ico.php" id="btn_retroceder_totem" class="btn_retroceder_totem"><i class="fa fa-arrow-left  fa-5x" style="color:white !important;margin-left:300px;margin-top:8px;"></i></a>
                  </div>      
            </div>
                                   
            </div>
</header>
<div id="confirma_reserva" style="background-color: #C2CCD1;
                                        background: linear-gradient(#444,#C2CCD1);">    
<div class="menu_login_confirma_reserva">                               
    <div class="logo_confirma_reserva">
    <?php
    session_start();
    $nombre_abonado = $_SESSION['NOMBRE'];
    $apellidos_abonado = $_SESSION['APELLIDOS'];
    $codigo_abonado = $_SESSION['CODIGO'];
    ?>
    
        <div class="datos_abonado">
        <label style="color:#C2CCD1;margin-bottom: 0.1em">Abonado nº:  <?php echo($codigo_abonado);?></label><br>
        <label style="color:#C2CCD1;margin-bottom: 0.1em">Nombre/Apellidos: <?php echo($nombre_abonado." ". $apellidos_abonado);?></label><hr>
        
        </div>
        <div id='ventanas_confirma_reserva' style="padding-left: 80px;max-width: 900px;max-height:500px !important;"></div>
        
    </div>
</div>
</div>
</div>


 
    
              
</body>
<script>
 $(document).ready(function(){
    $('#ventanas_confirma_reserva').load('mis_reservas_totem.php');
    

 });   


</script>    


</html>

