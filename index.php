<?php
include("funciones.php");
include("conexion.php");
ini_set("session.gc_maxlifetime", 60);
//Session_start();
//$nombre_socio= $_SESSION['NOMBRE'];
//$apellidos_socio = $_SESSION['APELLIDOS'];
//$socio = "Roberto";
//$apellidos_socio = "Hernandez";
//if($nombre_socio == '' || $nombre_socio = null){
//    echo("No se inicio sesion");
//    die();
//}
//echo $_SERVER['SERVER_ADDR'];



 
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewpoort" content="width=device-width,initial-scale=1.0">
    <!--<script type="text/javascript" src="librerias/javascript/jquery.js"></script>-->
    <link rel="stylesheet" href="estilos/estilos.css">
    <link rel="stylesheet" href="css/fontawesome-all.min.css">
     <!--librerias bootstrap-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="librerias/javascript/jquery.min.js"></script>
    <script src="librerias/javascript/moment.min.js"></script>
    <script src="librerias/javascript/jquery.min.js"></script>
    <script src="librerias/javascript/moment.min.js"></script>
    <!--full-calendar-->
    <link rel="stylesheet" href="librerias/css/fullcalendar.min.css">
    <script src="librerias/javascript/fullcalendar.min.js"></script>  
    <script src="librerias/javascript/es.js"></script> 
    <!--libreria js-->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
   
    <script language="JavaScript" SRC="funciones.js"></script>
   <!--ClockPickerr-->
   <link rel="stylesheet" href="librerias/css/bootstrap-clockpicker.css">
   <script src="librerias/javascript/bootstrap-clockpicker.js"></script>  
    
<!--<style>
label {color: black;}
.fc-button-prev.icon-prev {
    display: false;
}
</style> -->
</head>

<body>
<header>
    <div class="container-menu">

        <div class="logo">
            <div class="logo-name col-sm-10">
                <img src="imagenes/logo_bpx.png" alt="" style="border-radius:5px;">
                <label id="lab-tit-logo">Actividades</label>

            </div>
<!--            <div class="icon-menu col-sm-1">
                <a href="#" id="btn-menu" class="btn-menu"><span class="fa fa-bars"></span></a>

            </div>-->
            <div class="icon-usuario col-sm-2">
               <ul id="usuario-ul"> 
                   <ul>
                      <li>
                          <?php
                          session_start();
                          if (isset($_SESSION['NOMBRE'])){
                              
                           ?>  <a href="#">Hola, <?php echo $_SESSION['NOMBRE'];?> <span class="fa fa-user-friends"></span></a>
                           <ul>
                               <li><a href="#" id="click_mis_reservas" onclick="mis_reservas()">Mis Reservas</a></li>
                               <li> <a href="cerrar_sesion_socio.php" >Cerrar Sesión</a></li>
                           </ul>
                          <?PHP
                          }else{
                              ?> <a href="#" onclick="login_socio()">Iniciar Sesión  <span class="fa fa-user-friends" onclick="login_socio()"></span></a>
                           
                           <?php
                          }
                          ?>
                         
                           
                      </li>
                   </ul>
               <ul>    
            </div>    
         </div>  

    </div>
</header>
    
    

<div class="filtro-activi">
   <label>Filtrar</label>           
        <select id="actividades_selector" name="actividades_selector" style="margin-left:10px;">       
        <?php
        $conectando = conectar();
        $sql = "SELECT * FROM BPXPORT.ACTIVIDADES GROUP BY title";
        $consulta = mysqli_query($conectando, $sql);
         echo "(<option value='TODAS'>TODAS</option >'.'<br>')";
         while($row=mysqli_fetch_array($consulta)){
             $valor = $row['title'];
            echo "(<option value='$valor'>$valor</option >'.'<br>')";
         }
         ?>
        </select>
</div> 




    
<section class="banner">

<div class="ventana_socio" style="margin-top: 5px;color:black;float:left;clear:both">       
    <div class="row" style="max-width:100%">
        <!--<div class="col"></div>-->
        <div class="col-12"> <div id="calendario_actividades"></div></div>
        <!--<div class="col"></div>-->
    </div>
</div>
        
</section>


<script>
         
  $(document).ready(function(){
     
  
    filtro = $('select[name="actividades_selector"] option:selected').text();
   
    $('#calendario_actividades').fullCalendar({
    height: 650,
    width: 1900,
    defaultView: 'listWeek',
    editable:true,

    header: { 
        left: '',   
        center: 'title',
        right: 'listDay,listWeek',
    },
    views: { // set the view button names
        listWeek: {buttonText: 'Diario'}

    },
    //defaultView: 'agendaWeek',

//                dayClick:function(date,jsEvent,view){
////                    
//                    $("#txtFecha").val(date.format());
////                    
//                    $("#txtActividad").html(jsEvent.title);
//                    $("#txtMoniSala").html(jsEvent.descripcion);
////                    
//                    $("#txtColor").html(jsEvent.color); 
//                    $("#int_actividad").modal();
//                    
//                    },
//   events:'http://localhost/Bpxport/actividades_filtro.php?actividad='+filtro,
    events:'actividades.php',
//  
    eventClick:function(calEvent,jsEvent,view){                   

        $('#txtID').val(calEvent.id);
        $('#txtActividad').val(calEvent.title);
        $('#txtMoniSala').val(calEvent.descripcion);

        $('#txtColor').val(calEvent.color); 
         //En la variable FechaHora se divide con split porr un lado la fecha [0](recoge hasta el primer espacio) y por otro la hora [1](recoge despues del espacio)
        FechaHora = calEvent.start._i.split(" ");


        $('#txtFecha').val(FechaHora[0]);
        $('#txtHora').val(FechaHora[1]);

        $("#Modal_reservas").modal();

    },
    eventRender: function(event, element,view) { 
//      element.find('.fc-title').append("<br/>" + event.descripcion); 
        return['TODAS',event.title].indexOf($("#actividades_selector").val()) >=0
     } 
  });
    
   $("#actividades_selector").change(function(){ 
//      var filtro = $("#actividades_selector").val();
//        $.post('actividades_filtro.php', {actividad: filtro}, function(resultado){ 
//            alert(resultado);
             $('#calendario_actividades').fullCalendar('rerenderEvents');   
            
//        });
    });
   
  
      
      
   });
    

</script> 

   
    
<script>      
function login_socio(){
    $('#login_socio').modal('toggle');
 }
 function mis_reservas(){       
    window.open('mis_reservas.php','','width=800,height=600,top=210px,left=200px,location=no,statusbar=no,toolbar=no,scrollbars=yes,navbar=no,menubar=no,resizable=no'); 
    $("#modal_mis_reservas").modal('toggle');      
 }
 function mostrar_registro_provisional(){
     
   
     $('#modal_registro_provisional').modal('toggle');
}

</script>
    
      <!--Conexion autenticacion usuarios-->
   
    <div class="modal fade" id="login_socio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="tituloEvento" style="color:black">Login</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <center>
          <div class="modal-body">
              <input type="hidden" id="txtID" name="txtID" />
              <input type="hidden" id="txtFecha" name="txtFecha" />
              
              <div class="form-horizontal">
                <div class="form-group">
                    <!--<label>Usuario</label>-->
                    <input id="nif_socio" name ="nif_socio" placeholder="DNI/NIF"  style="text-align: center" value="" />
                </div>
 
                <div class="form-group">   
                    <!--<label>Contraseña</label>-->
                    <input type="password" id="movil_socio" name = "movil_socio" style="text-align: center" placeholder="Movil" value="" />    
                </div>
                <div>
                    <a href="#" onclick="mostrar_registro_provisional();">¿Aún no tienes usuario? Registrate</a>
                </div>
               </div> 
              
          </div>
           </center>   
          <div class="modal-footer">
            <button type="button" class="btn btn-block" id="btnAceptar" onclick="autentificar_socio('online')">Login</button>
            
            <!--<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>-->
          </div>
        </div>
      </div>
    </div>
    
    <!--modal para reservar una actividad-->
    <div class="modal fade" id="Modal_reservas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
           <!--Se define el tamaño de la ventana-->
            <div class="modal-dialog" role="document">
                <!--Se define estilos de la ventana fondo, bordes, sombreado-->
                    <div class="modal-content">
                            <!--Se define el boton de cerrar y el titulo-->
                            <div class="modal-header">
                                <h5 class="modal-title" style="color:black">Reserva de Actividad</h5>
                                
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><br>
                                        <span aria-hidden="true">&times;</span>        
                                </button>
                    
                            </div>
                            <!--Se define el contenido del modal-->
                            <div class="modal-body">
                                <input type="hidden" id="txNombreSocio" name="txNombreSocio" value = "<?php echo  $_SESSION['NOMBRE'];?>"/>
                                <input type="hidden" id="txtID" name="txtID" />
                                <!--<input type="hidden" id="txtFecha" name="txtFecha" />-->
                               <input type="hidden" id="txtcodigoSocio" name="txtcodigoSocio" value = "<?php echo $_SESSION['CODIGO'];?>"/>  
                                <div class="form-row">
                                  <div class="form-group col-sm-8">
                                       <label>Actividad</label>
                                       <input type="text" id="txtActividad" name="txtActividad" class="form-control" placeholder="Actividad" disabled/>
                                  </div>
                                  <div class="form-group col-sm-4">
                                       <label>Hora</label>
                                       <div class="input-group clockpicker" data-autoclose="true">
                                          <input type="text" id="txtHora" value="10:30" class="form-control" disabled/>
                                       </div>
                                  </div>
                                </div>   
                                <div class="form-group">   
                                    <label>Monitor/Sala</label>
                                    <textarea id="txtMoniSala" name="txtMoniSala" row="3" class="form-control" disabled></textarea>     
                                </div>
                                
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" onclick="verificar_autenti('online')">Reservar</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            </div>
                          
                    </div>
            </div>
    </div>
    
      <!--modal para enviar registro provisional de socio-->
    <div class="modal fade" id="modal_registro_provisional" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
           <!--Se define el tamaño de la ventana-->
        <div class="modal-dialog" role="document">
        <!--Se define estilos de la ventana fondo, bordes, sombreado-->
            <div class="modal-content">
                <!--Se define el boton de cerrar y el titulo-->
                <div class="modal-header">
                    <h5 class="modal-title" style="color:black">Nuevo Usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>        
                    </button>

                </div>
                
                <!--Se define el contenido del modal-->
                <div class="modal-body">
                  
                    <div class="form-row">  
                        <div class="form-group col-sm-6">   
                            <label>NOMBRE</label>
                            <input id="txtnombre" name="txtnombre" class="form-control" />
                        </div>
                        <div class="form-group col-sm-6">   
                            <label>APELLIDOS</label>
                            <input id="txtapel" name="txtapel" class="form-control" />
                        </div>
                    </div>    
                    <div class="form-row">     
                        <div class="form-group col-sm-4">   
                            <label>NIF</label>
                            <input id="txtnif" name="txtnif" class="form-control" />
                        </div>
                    
                        
                        <div class="form-group col-sm-4">   
                            <label>MOVIL</label>
                            <input id="txtmovil" name="txtmovil" class="form-control" />
                        </div>
                                       
                        <div class="form-group col-sm-4">   
                            <label>FIJO</label>
                            <input id="txttel1" name="txttel1" class="form-control" />
                        </div>
                    </div>
                     <div class="form-row"> 
                        <div class="form-group col-sm-12">
                            <label>EMAIL</label>
                            <input id="txtmail" name="txtmail" class="form-control" />
                        </div>
                        
                    </div>
                
                    
                <div class="modal-footer">
                     <div class="form-group col-sm-12" id="codigo_verificacion" style="display:none;">
                             <label style="text-align:center;width:100%;">Introduzca el Código que le hemos enviado a su email</label>
                            <input id="txtcodigoverifi" name="txtcodigoverifi" class="form-control" style="margin: 5px;"/>
                            <button type="button" class="btn btn-block" id="btnvalidarcodigo" style="background-color:#8fdf82 !important;color:white;" onclick="validar_registro_online()">validar</button>
                     </div>       
                <button type="button" class="btn btn-block" id="btnEnviar" style="background-color: #2aabd2!important;color:white;" onclick="correo_codigo_aleatorio()">ENVIAR</button>
                
              </div>

            </div>
        </div>
    </div>
    
    </div>
    
    
    
    
<script>
//    La sacamos fuera para poder utilizarla en todo momento 
  
var Nuevo_Evento; 
 //var filtro = $('#actividades_selector').val();
$("#boton-filtro").click(function(){ 
     //filtro = $('select[name="actividades_selector"] option:selected').text();
    //$('#calendario_actividades').fullCalendar('refetchEvents');
   
    
    RecolectarDatosfiltros();
    EnviarInformacion('filtrar',Nuevo_Evento);
   
});
 function RecolectarDatosfiltros(){
     
        Nuevo_Evento = {
            
            filtro:$('#actividades_selector').val(),
               
        };
        
    }
function EnviarInformacion(accion,objEvento){
        $.ajax({
           type:'POST',
           url:'actividades.php?accion='+accion,
          data:objEvento,
           success:function(msg){
            
                    $('#calendario_actividades').fullCalendar('refetchEvents')
                    

             
           },
           error:function(){
               alert("hay un error...");
           }
           
        });
    }
        //Ponemos a la clase clockpicker el selector clockpicker
        //$('.clockpicker').clockpicker();
 
function correo_codigo_aleatorio(){
    var dir_mail =  $('#txtmail').val();
    var nif =  $('#txtnif').val();
   
    $.post('ajax.php', {clave_aleatoria: dir_mail,nif:nif}, function(respuesta){   
       if(respuesta){
           $('#btnEnviar').fadeOut('.1s','swing');
           $('#codigo_verificacion').fadeIn('.2s','swing');
            
           
//           var int_codigo = prompt("Compruebe su cuenta de correo e introduzca el codigo de validación.");
//           var strint_codigo = string(int_codigo);
//           var strrespuesta = string(respuesta);
//           
//           if (strrespuesta === strint_codigo) {
//                 alert("codigo correcto");
//            }else{
//                alert("codigo INcorrecto");
//           }
       }
        
        
    }); 
}
function validar_registro_online(){
    var int_clave = $('#txtcodigoverifi').val();
    var nombre =  $('#txtnombre').val();
    var apel =  $('#txtapel').val();
    var nif = $('#txtnif').val();
    var movil = $('#txtmovil').val();
    var tel = $('#txttel1').val();
    var mail = $('#txtmail').val();
    $.post('ajax.php', {clave_verifi: int_clave,nombre:nombre,apel:apel,nif:nif,movil:movil,tel:tel,mail:mail}, function(respuesta){ 
        if(respuesta){
         alert(respuesta);
        }   
    });
}


</script>
    
</body>



</html>

