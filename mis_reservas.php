<?php
include("funciones.php");
ini_set("session.gc_maxlifetime", 60);
session_start();
$socio = $_SESSION['CODIGO'];
?>
<html>
<head>
 <meta charset="UTF-8">
    <meta name="viewpoort" content="width=device-width,initial-scale=1.0">
    <!--<script type="text/javascript" src="librerias/javascript/jquery.js"></script>-->
    <link rel="stylesheet" href="estilos/estilos.css">
    <link rel="stylesheet" href="css/fontawesome-all.min.css">
     <!--librerias bootstrap-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="librerias/javascript/jquery.min.js"></script>
    <script src="librerias/javascript/moment.min.js"></script>
   
    <!--full-calendar-->
    <link rel="stylesheet" href="librerias/css/fullcalendar.min.css">
    <script src="librerias/javascript/fullcalendar.min.js"></script>  
    <script src="librerias/javascript/es.js"></script> 
    <!--libreria js-->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
   
    <script language="JavaScript" SRC="funciones.js"></script>
   <!--ClockPickerr-->
   <link rel="stylesheet" href="librerias/css/bootstrap-clockpicker.css">
   <script src="librerias/javascript/bootstrap-clockpicker.js"></script>  
</head>   
<body>   
  <!--modal para visualizar el estado de las reservas de Actividad-->
    <div class="" id="modal_mis_reservas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
           <!--Se define el tamaño de la ventana-->
        <div class="modal-body" role="document">
        <!--Se define estilos de la ventana fondo, bordes, sombreado-->
            <div class="modal-content">
                <!--Se define el boton de cerrar y el titulo-->
                <div class="modal-header">
                    <h5 class="modal-title" style="color:black">Mis Reservas</h5>
<!--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><br>
                            <span aria-hidden="true">&times;</span>        
                    </button>-->
                </div>
                <!--Se define el contenido del modal-->
                <div class="modal-body">
                    <input type="hidden" id="txNombreSocio" name="txNombreSocio" value = "<?php echo  $_SESSION['NOMBRE'];?>"/>
                    <input type="hidden" id="txtID" name="txtID" />
                    
                    <input type="hidden" id="txtFecha" name="txtFecha" />
                   <input type="hidden" id="txtcodigoSocio" name="txtcodigoSocio" value = "<?php echo $_SESSION['CODIGO'];?>"/>  

                      <div class="form-group" style="margin-left:5px;">
                          <label>Socio nº : <?php echo $_SESSION['CODIGO'];?></label><br>
                          <!--<input type="text" id="n_Socio" name="n_Socio" value="<?php echo $_SESSION['CODIGO'];?>" disabled/><br>-->
                         <label>Nombre : <?php echo $_SESSION['NOMBRE'];?></label><br>
                            <!--<input type="text" id="cod_Socio" name="cod_Socio" value="<?php echo $_SESSION['NOMBRE'];?>" disabled/>-->
                      </div>

                    <div class="form-group table-responsive">   
                        <table class="table table-hover">
                        <tr><td>Fecha Reserva</td><td>Actividad</td><td>Fecha</td><td>Confirmada</td><td></td><td></td></tr>

                        <?php
                            $conectando = conectar();
                            $sql ="SELECT * FROM BPXPORT.RESERVAS WHERE SOCIO = $socio";
                            $result = mysqli_query($conectando, $sql);

                            // comienza un bucle que leerá todos los registros existentes
                            while($row = mysqli_fetch_array($result)) {
                                $fecha_reserva = $row['FECHA'];
                                $id_actividad = $row['ID_ACTIVIDAD'];
                                //Extraemos la fecha de la actividad para mostrarala en el listado de mis reservas
                                $sql_fecha = "SELECT id,start FROM BPXPORT.ACTIVIDADES where id = '$id_actividad'";
                                $resul_fecha = mysqli_query($conectando, $sql_fecha);
                                $fila_fecha = mysqli_fetch_row($resul_fecha);
                                if(mysqli_num_rows($resul_fecha) > 0){
                                    $fecha_actividad = $fila_fecha[1]; 
                                }
                                
                                $actividad_confirmada= $row['CONFIRMADA'];
                                if ($actividad_confirmada == '0'){
                                    $clase_confirma = "rojo";
                                }else {
                                    $clase_confirma = "verde";
                                }
                                //capturar la hora de la actividad
                                

                        ?>
                        <tr class="<?php echo $clase_confirma; ?>" id="linea_reserva"><td><?php echo $fecha_reserva; ?></td><td><?php texto_actividad($id_actividad); ?></td><td><?php echo $fecha_actividad;?></td><td><?php texto_confirmada($actividad_confirmada); ?></td>
                            <!--<td><span id ="icon-elim-reserva" class="fa fa-trash-alt" onclick="elimina_reserva(<?php echo $id_actividad?>,<?php echo $socio ?>)"></span></td><td><span id ="icon-confirm-reserva" class="fa fa-check-circle" onclick="confirma_reserva(<?php echo $id_actividad?>,<?php echo $socio ?>)"></span></td></tr>-->                                            
                            <!--echo "<div class='linea'><tr class=''><td>$fecha_reserva</td><td>texto_actividad($id_actividad)</td><td>$actividad_confirmada</td></tr</div>";-->
                          <?php 

                            }

                            mysql_free_result($result); // Liberamos los registros
                            mysql_close($conectando); // Cerramos la conexion con la base de datos
                           ?>

                        </table>         
                    </div>
                   
                </div>
                <div class="modal-footer">
                   <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>-->
                </div>

            </div>
        </div>
    </div>
    
                        
          </div> 
        </div>
      </div>
 </div>
      
</body>
</html>

