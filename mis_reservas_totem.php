<?php
include("funciones.php");
ini_set("session.gc_maxlifetime", 60);
session_start();
$socio = $_SESSION['CODIGO'];
?>
<html>
<head>
 <meta charset="UTF-8">
    <meta name="viewpoort" content="width=device-width,initial-scale=1.0">
    <!--<script type="text/javascript" src="librerias/javascript/jquery.js"></script>-->
    <link rel="stylesheet" href="estilos/estilos.css">
    <link rel="stylesheet" href="css/fontawesome-all.min.css">
     <!--librerias bootstrap-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="librerias/javascript/jquery.min.js"></script>
    <script src="librerias/javascript/moment.min.js"></script>
   
    <!--full-calendar-->
    <link rel="stylesheet" href="librerias/css/fullcalendar.min.css">
    <script src="librerias/javascript/fullcalendar.min.js"></script>  
    <script src="librerias/javascript/es.js"></script> 
    <!--libreria js-->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
   
    <script language="JavaScript" SRC="funciones.js"></script>
   <!--ClockPickerr-->
   <link rel="stylesheet" href="librerias/css/bootstrap-clockpicker.css">
   <script src="librerias/javascript/bootstrap-clockpicker.js"></script>  
</head>  

<body>   
  <!--modal para visualizar el estado de las reservas de Actividad-->
    <div class="" id="modal_mis_reservas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
           <!--Se define el tamaño de la ventana-->
        <div class="modal-body" role="document">
        <!--Se define estilos de la ventana fondo, bordes, sombreado-->
            <div class="modal-content">
                <!--Se define el boton de cerrar y el titulo-->
                <div class="modal-header">
                    <h5 class="modal-title" style="color:black;font-size: 25px;">Mis Reservas</h5>
<!--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><br>
                            <span aria-hidden="true">&times;</span>        
                    </button>-->
                </div>
                <!--Se define el contenido del modal-->
                <div class="modal-body">
                    <input type="hidden" id="txNombreSocio" name="txNombreSocio" value = "<?php echo  $_SESSION['NOMBRE'];?>"/>
                    <input type="hidden" id="txtID" name="txtID" />
                    
                    <!--<input type="hidden" id="txtFecha" name="txtFecha" />-->
                   <input type="hidden" id="txtcodigoSocio" name="txtcodigoSocio" value = "<?php echo $_SESSION['CODIGO'];?>"/>  
 

                    <div class="form-group table-responsive">   
                        <table class="table table-hover" style="font-size: 22px;">
                        <tr><td>Fecha Reserva</td><td>Actividad</td><td>Fecha Actividad</td><td>Confirmada</td></tr>

                        <?php
                            
//                            $ahora = new DateTime(date("Y-m-d H:i:s"));
                            $fecha_ahora = date("Y-m-d");
                            $hora_ahora = date("H:i:s");
                            $conectando = conectar();
                            
                            $sql ="SELECT BPXPORT.RESERVAS.ID_ACTIVIDAD, BPXPORT.RESERVAS.SOCIO, BPXPORT.RESERVAS.FECHA,BPXPORT.RESERVAS.CONFIRMADA,
                            BPXPORT.ACTIVIDADES.id, BPXPORT.ACTIVIDADES.start 
                            FROM BPXPORT.RESERVAS, BPXPORT.ACTIVIDADES 
                            WHERE BPXPORT.RESERVAS.ID_ACTIVIDAD = BPXPORT.ACTIVIDADES.id
                            AND BPXPORT.RESERVAS.SOCIO = '$socio'
                            AND DATE_FORMAT(BPXPORT.ACTIVIDADES.start, '%Y-%m-%d') = '$fecha_ahora'
                            AND DATE_FORMAT(BPXPORT.ACTIVIDADES.start, '%H:%i:%s') >= '$hora_ahora'";
                            
//                            $sql ="SELECT * FROM BPXPORT.RESERVAS WHERE SOCIO = '$socio'";
                            $result = mysqli_query($conectando, $sql);

                            // comienza un bucle que leerá todos los registros existentes
                            while($row = mysqli_fetch_array($result)) {
                                
                                $fecha_reserva = $row['FECHA'];
                                $id_actividad = $row['ID_ACTIVIDAD'];
                                $actividad_confirmada = $row['CONFIRMADA'];
                               
                              //$fecha_actividad = date('Y-m-d',$row['start']);
                                $fecha_actividad = $row['start'];
                                $traer_hora_actividad = explode(" ",$row['start']); 
                                $hora_actividad = $traer_hora_actividad[1]; 
                                
                             
                                
                                if ($actividad_confirmada == '0'){
                                    $clase_confirma = "rojo";
                                }else {
                                    $clase_confirma = "verde";
                                    
                                }
//                                $dentro_de_horario = franja_horaria($hora_ahora,$hora_actividad);
////                                //capturar la hora de la actividad
//                                 if ($dentro_de_horario == false){
////                                if (franja_horaria($hora_ahora,$hora_actividad) === 'false'){
////                                    $deshabilitar_reg = 'deshabilita_reg_confirmacion';
//                                   $deshabilitar_reg = 'deshabilita_reg_confirmacion';
//                                   
//                                }else{    
//                                    
//                                }

                        ?>
                        <tr class="<?php echo $clase_confirma; ?>" id="linea_reserva" style="font-size: 18px;"><td style="vertical-align:middle;text-align: left;"><?php echo $fecha_reserva; ?></td><td style="vertical-align:middle;text-align: left;"><?php texto_actividad($id_actividad); ?></td><td style="vertical-align:middle;text-align: left;"><?php echo $fecha_actividad; ?></td><td style="vertical-align:middle;text-align: center;"><?php texto_confirmada($actividad_confirmada); ?></td>
                            <?php 
                                if($clase_confirma == "rojo"){
                                
                                ?>
                                <td style="background-color: white !important;color:#8699A4 !important;font-size: 40px; "><span id ="icon-confirm-reserva" class="fa fa-check-circle" onclick="confirma_reserva_totem(<?php echo $id_actividad;?>,<?php echo $socio;?>)"></span></td></tr>  
                                                                                   
                        
                            <!--echo "<div class='linea'><tr class=''><td>$fecha_reserva</td><td>texto_actividad($id_actividad)</td><td>$actividad_confirmada</td></tr</div>";-->
                          <?php 

                                }
                            }
                            mysql_free_result($result); // Liberamos los registros
                            mysql_close($conectando); // Cerramos la conexion con la base de datos
                           ?>

                        </table>         
                    </div>
                   
                </div>
                <div class="modal-footer">
                   <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>-->
                </div>

            </div>
        </div>
    </div>
    
                        

</body>

</html>

