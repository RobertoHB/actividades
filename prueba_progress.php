<!--<!DOCTYPE html> 

<!DOCTYPE html> 
<!--<form name="formulario" method="post" action="http://pagina.com/send.php">
   Medidor 
  <meter min="0" max="100"
         low="25" high="75"
         optimum="100" value="75">
</form>-->
<!--<html> 
<body> 
    <div class="progress">
        <progress id="progresando" max="100" value="0"></progress>
    <span class="progress-value"></span>%
    </div>

</body> 
<script src="librerias/javascript/jquery.min.js"></script>    
</html>

<script>
    
    $(document).ready(function() {  
        var progressbar = $('#progresando'),  
            max = progressbar.attr('max'),  
            time = (1000/max)*5,      
            value = progressbar.val();  
     
        var loading = function() {  
            value += 1;  
            addValue = progressbar.val(value);  
             
            $('.progress-value').html(value + '%');  
     
            if (value == max) {  
                clearInterval(animate);                      
            }  
        };  
     
        var animate = setInterval(function() {  
            loading();  
        }, time);  
    };

</script>-->
<!DOCTYPE html> 
<br />
<br />
<html>
<body>
<div class="container">
 <div class="panel panel-primary">
      <div class="centrado panel-heading">
        <h3 class="panel-title">(Animated Progress Bar) - Barra de progreso animada </h3>
        <!-- titulo-->
      </div>
      <div class="panel-body">
        <div class="jbotmio">
          <br>

          <div class="progress progress-striped active">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">

              <span class="sr-only">90% Complete</span>
            </div>
          </div>


          <div class="progress progress-striped active">
            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">

              <span class="sr-only">90% Complete</span>
            </div>
          </div>

          <div class="progress progress-striped active">
            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">

              <span class="sr-only">40% Complete</span>
            </div>
          </div>

          <div class="progress progress-striped active">
            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 10%;">

              <span class="sr-only">10% Complete</span>
            </div>
          </div>
          <br />
          
          <footer>
        
      <div class="container footer">
        <small><span class="glyphicon glyphicon-road"></span><em> By StraKnow</em></small>
      </div>

    </footer>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</div>
</div>