<?php
include("funciones.php");


?>
<html>
<head>
 <meta charset="UTF-8">
    <meta name="viewpoort" content="width=device-width,initial-scale=1.0">
    
    <link rel="stylesheet" href="estilos/estilos.css">
    <link rel="stylesheet" href="css/fontawesome-all.min.css">
    
    
     <!--css que contiene las CSS del autocomplete-->
     <link rel="stylesheet" href="librerias/css/easy-autocomplete.css">
     <link rel="stylesheet" href="librerias/css/easy-autocomplete.themes.css">
    
     <!--librerias bootstrap-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="librerias/javascript/jquery.min.js"></script>
    <script src="librerias/javascript/moment.min.js"></script>
    <script src="librerias/javascript/jquery.min.js"></script>
    <script src="librerias/javascript/moment.min.js"></script>
    
    <script src="librerias/javascript/jquery.easy-autocomplete.min.js"></script> 
    
   
    <script src="librerias/javascript/es.js"></script> 
    <!--libreria js-->
    
   
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
   
    <script language="JavaScript" SRC="funciones.js"></script>
   
</head>   
<body>   
  <!--modal para enviar registro provisional de socio-->
    <div class="" id="modal_registro_provisional" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-color: #EEE">
           <!--Se define el tamaño de la ventana-->
        <div class="modal-dialog" role="document">
        <!--Se define estilos de la ventana fondo, bordes, sombreado-->
            <div class="modal-content">
                <!--Se define el boton de cerrar y el titulo-->
                <div class="modal-header">
                    <h5 class="modal-title" style="color:black">Nuevo Usuario</h5>

                </div>
                
                <!--Se define el contenido del modal-->
                <div class="modal-body">
                  
                    <div class="form-row">  
                        <div class="form-group col-sm-12">   
                            <label>Nombre</label>
                            <input id="txtnombre" name="txtnombre" class="form-control" />
                        </div>
                        <div class="form-group col-sm-12">   
                            <label>Apellidos</label>
                            <input id="txtapel" name="txtapel" class="form-control" />
                        </div>
                        <div class="form-group col-sm-12">   
                            <label>Nif</label>
                            <input id="txtnif" name="txtnif" class="form-control" />
                        </div>
                    
                        
                        <div class="form-group col-sm-12">   
                            <label>Movil</label>
                            <input id="txtmovil" name="txtmovil" class="form-control" />
                        </div>
                                       
                        <div class="form-group col-sm-12">   
                            <label>Telefono 1</label>
                            <input id="txttel1" name="txttel1" class="form-control" />
                        </div>
                        
                        <div class="form-group col-sm-12">
                            <label>Email</label>
                            <input id="txtmail" name="txtmail" class="form-control" />
                        </div>
                        
                    </div>
                
                    
                <div class="modal-footer">
                
                <button type="button" class="btn btn-success" id="btnEnviar">Enviar</button>
                
              </div>

            </div>
        </div>
    </div>
    
    </div>
      
</body>
<script>

//    La sacamos fuera para poder utilizarla en todo momento 
   

        $("#btnEnviar").click(function(){
          
            EnviarDatos_Socio('agregar');
        });
     
        
   
         
    
function EnviarDatos_Socio(accion){
    var codigo = $('#txtcodigo').val();
    var codigo_pul= $('#txtcodigopul').val();
    var nombre = $('#txtnombre').val();
    var apellidos = $('#txtapel').val();
    var nif = $('#txtnif').val();
    var movil = $('#txtmovil').val();
    var tel1 = $('#txttel1').val();
    var tel2 = $('#txttel2').val();
    var mail = $('#txtmail').val();
    $.post('ajax.php', {accion_socios: accion,codigo:codigo,codigo_pul:codigo_pul,nombre:nombre,apellidos:apellidos,nif:nif,movil:movil,tel1:tel1,tel2:tel2,mail:mail}, function(mensaje) { 
     alert(mensaje);
     $("#socios_selector").val($('option:first', select).val());
    });
}

</script>
</html>
