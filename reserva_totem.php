<?php
include("funciones.php");
include("conexion.php");
ini_set("session.gc_maxlifetime", 60);

?>
<html>
<head>
  <meta charset="UTF-8">
    <meta name="viewpoort" content="width=device-width,user-scalable=no, initial-scale=1.0,
          maximum-scale=1.0, minimum-scale=1.0">
    <!--librerias jquery-->
     <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="librerias/javascript/jquery.easy-autocomplete.min.js"></script> 
     <!--css que contiene las CSS del autocomplete-->
     <link rel="stylesheet" href="librerias/css/easy-autocomplete.css">
     <link rel="stylesheet" href="librerias/css/easy-autocomplete.themes.css">
     
   <!--librerias bootstrap-->
   <link rel="stylesheet" href="estilos/estilos.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    
    
    <!--<script type="text/javascript" src="librerias/javascript/jquery.js"></script>-->
     <link rel="stylesheet" href="estilos/estilos.css">
     
     <!--css que contiene las fuentes e iconos de la app-->
     <link rel="stylesheet" href="css/fontawesome-all.min.css">
     
    <script src="librerias/javascript/moment.min.js"></script>

    <!--full-calendar-->
    <link rel="stylesheet" href="librerias/css/fullcalendar.min.css">
    <script src="librerias/javascript/fullcalendar.min.js"></script>  
    <script src="librerias/javascript/es.js"></script> 
    <!--libreria js-->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    
        <!--autocomplete-->
   

    <script language="JavaScript" SRC="funciones.js"></script>
   <!--ClockPickerr-->
   <link rel="stylesheet" href="librerias/css/bootstrap-clockpicker.css">
   <script src="librerias/javascript/bootstrap-clockpicker.js"></script>  
    
</head>  


<body>   
<header>
    <div class="container-menu">                               
            <div class="logo">
                <div class="logo-name col-sm-6">
                    <img src="imagenes/logo_bpx.png" alt="" style="width: 65%;margin-left:40px;">
                </div>   
<!--                <div class="logo-name col-sm-5">
                    <p style="color:white;font-weight: bold;font-family: fantasy; padding-top:25px;font-size:16px">Confirmación de Actividades</p>
                </div>   -->
                  <div class="logo-name col-sm-6">   
                      <a href="confirma_reserva_ico.php" id="btn_retroceder_totem" class="btn_retroceder_totem"><i class="fa fa-arrow-left  fa-5x" style="color:white !important;margin-left:300px;margin-top:8px;"></i></a>
                  </div>      
            </div>
                                   
            </div>
</header>
<div id="confirma_reserva" style="background-color: #C2CCD1;
                                        background: linear-gradient(#444,#C2CCD1);">    
<div class="menu_login_confirma_reserva">                               
    <div class="logo_confirma_reserva">
    <?php
    session_start();
    $nombre_abonado = $_SESSION['NOMBRE'];
    $apellidos_abonado = $_SESSION['APELLIDOS'];
    $codigo_abonado = $_SESSION['CODIGO'];
    ?>
    
        <div class="datos_abonado">
        <label style="color:#C2CCD1;margin-bottom: 0.1em">Abonado nº:  <?php echo($codigo_abonado);?></label><br>
        <label style="color:#C2CCD1;margin-bottom: 0.1em">Nombre/Apellidos: <?php echo($nombre_abonado." ". $apellidos_abonado);?></label><hr>
        
        </div>
        <div id='ventanas_confirma_reserva' style="padding-left: 40px;max-width: 850px;max-height:300px !important;"></div>
        
    </div>
</div>
</div>
  <!--modal para reservar una actividad-->

<div class="modal fade contenedor_modal" id="Modal_reservas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <!--Se define el tamaño de la ventana-->
    <div class="modal-dialog modal-lg" role="document">
         <!--Se define estilos de la ventana fondo, bordes, sombreado-->
        <div class="modal-content">
            <!--Se define el boton de cerrar y el titulo-->
            <div class="modal-header contenedor_modal">
                <h5 class="modal-title" style="color:black">Reserva de Actividad</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><br>
                        <span aria-hidden="true">&times;</span>        
                </button>

            </div>
            <!--Se define el contenido del modal-->
            <div class="modal-body contenedor_modal">
                <input type="hidden" id="txNombreSocio" name="txNombreSocio" value = "<?php echo  $_SESSION['NOMBRE'];?>"/>
                <input type="hidden" id="txtID" name="txtID" />
                <input type="hidden" id="txtFecha" name="txtFecha" />
                <input type="hidden" id="txtcodigoSocio" name="txtcodigoSocio" value = "<?php echo $_SESSION['CODIGO'];?>"/>  
                <div class="form-row contenedor_modal">
                  <div class="form-group col-sm-8">
                       <label>Actividad</label>
                       <input type="text" id="txtActividad" name="txtActividad" class="form-control" placeholder="Actividad" disabled/>
                  </div>
                  <div class="form-group col-sm-4">
                       <label>Hora</label>
                       <div class="input-group clockpicker" data-autoclose="true">
                          <input type="text" id="txtHora" value="10:30" class="form-control" disabled/>
                       </div>
                  </div>
                </div>   
                <div class="form-group">   
                    <label>Monitor/Sala</label>
                    <textarea id="txtMoniSala" name="txtMoniSala" row="3" class="form-control" disabled></textarea>     
                </div>

            </div>
            <div class="modal-footer contenedor_modal">
                <button type="button" class="btn btn-primary" onclick="verificar_autenti('totem')">Reservar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>

            </div>
    </div>
</div>


 
    
              
</body>
<script>
 $(document).ready(function(){
    $('#ventanas_confirma_reserva').fullCalendar({
    height: 1350,
    
    defaultView: 'listWeek',
   
    editable:true,
    

    header: { 
        left: '',   
        center: '',
       
        right: '',
    },
    views: { // set the view button names
        listWeek: {buttonText: 'Diario'}

    },
   
    events:'actividades.php',

    eventClick:function(calEvent,jsEvent,view){                   

        $('#txtID').val(calEvent.id);
        $('#txtActividad').val(calEvent.title);
        $('#txtMoniSala').val(calEvent.descripcion);

        $('#txtColor').val(calEvent.color); 
         //En la variable FechaHora se divide con split porr un lado la fecha [0](recoge hasta el primer espacio) y por otro la hora [1](recoge despues del espacio)
        FechaHora = calEvent.start._i.split(" ");


        $('#txtFecha').val(FechaHora[0]);
        $('#txtHora').val(FechaHora[1]);

        $("#Modal_reservas").modal();

    }
 
      
      
   });
    $('#ventanas_confirma_reserva').css({"width" : "100%","font-size" : "30","margin-left":"50"});
 });   


</script>    


</html>

