<?php
include("funciones.php");
ini_set("session.gc_maxlifetime", 60);
if($_GET['filtro']){
    $filtro_reservas = $_GET['filtro'];
}

?>
<!DOCTYPE html>    
<html>
<head>
 <meta charset="UTF-8">
    <meta name="viewpoort" content="width=device-width,initial-scale=1.0">
    <!--<script type="text/javascript" src="librerias/javascript/jquery.js"></script>-->
    
    <link rel="stylesheet" href="estilos/estilos.css">
    <link rel="stylesheet" href="css/fontawesome-all.min.css">
    <script language="JavaScript" SRC="funciones.js"></script>
    
    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="librerias/javascript/jquery.easy-autocomplete.min.js"></script> 
    <script src="librerias/javascript/jquery-ui.min.js"></script> 
    
     
     <!--css que contiene las CSS del autocomplete-->
     <link rel="stylesheet" href="librerias/css/easy-autocomplete.css">
     <link rel="stylesheet" href="librerias/css/easy-autocomplete.themes.css">
     
   <!--librerias bootstrap-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    
    
    <!--<script type="text/javascript" src="librerias/javascript/jquery.js"></script>-->
     <link rel="stylesheet" href="estilos/estilos.css">
     
     <!--css que contiene las fuentes e iconos de la app-->
     <link rel="stylesheet" href="css/fontawesome-all.min.css">
 
    <script src="librerias/javascript/moment.min.js"></script>

   
    <!--libreria js-->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    
        <!--autocomplete-->
   

   
   <!--ClockPickerr-->
   <link rel="stylesheet" href="librerias/css/bootstrap-clockpicker.css">
   <script src="librerias/javascript/bootstrap-clockpicker.js"></script> 
   
   <link rel="stylesheet" href="librerias/css/bootstrap-datepicker.css">
   <script src="librerias/javascript/bootstrap-datepicker.min.js"></script>  
   
    
    
<script type="text/javascript">
$(function() {
//    $("#date-pic").datepicker();
//    $("#date-pic").datepicker();
    $('#date-pic').datepicker({ dateFormat: 'YYYY-mm-dd' });
    
    
//    $("#date-pic").datepicker({
//      changeMonth: true,
//      changeYear: true,
//      yearRange: '1900:+0',
//      defaultDate: '',
//      buttonImage: "http://www.theplazaclub.com/club/images/calendar/outlook_calendar.gif",
//      dateFormat: 'yyyy-mm-dd',
//      onSelect: function() {
//        $('#date-pic').val($(this).datepicker({
//          dateFormat: 'yyyy-mm-dd'
//        }).val());
//      }
//    });
//$(function() {
//    $( "#date-pic" ).datepicker({
//        minDate: -100,
//        maxDate: "+0D",
//        dateFormat: 'yy-mm-dd',
//        onSelect: function(datetext){
//            $(this).val(datetext);
//        },
//    });
//});
//
//$.datepicker.setDefaults($.datepicker.regional["es"]);
//    $("#date-pic").datepicker();
});
 

</script> 
<script src="datepicker/dist/locales/bootstrap-datepicker.es.min.js"></script> 
  
   
</head>   
<body>   
    <div class="form-group">
    <label>Fecha</label>
        <input id = "date-pic" data-date-format = "yyyy-mm-dd"/>
        <button id="filtrar_fechas" class="btn-default" onclick="filtrar_reservas()">Filtrar</button>
        </div>
 



<div class="form-group table-responsive">   
  <table class="table" id="tabla-reservas" style="border:0 !important; width:1000px !important;" ">
  

  <?php
      $conectando = conectar();
      if (isset($filtro_reservas)){
          $filtro_fecha = " AND DATE_FORMAT(BPXPORT.ACTIVIDADES.start, '%Y-%m-%d') = '$filtro_reservas'";
      }
      $sql ="SELECT BPXPORT.RESERVAS.ID_ACTIVIDAD, BPXPORT.RESERVAS.SOCIO, BPXPORT.RESERVAS.FECHA,BPXPORT.RESERVAS.CONFIRMADA,
                            BPXPORT.SOCIOS.CODIGO, BPXPORT.SOCIOS.NOMBRE, BPXPORT.SOCIOS.APELLIDOS, 
                            BPXPORT.ACTIVIDADES.id, BPXPORT.ACTIVIDADES.start,BPXPORT.ACTIVIDADES.title, BPXPORT.ACTIVIDADES.plazas 
                            FROM BPXPORT.RESERVAS, BPXPORT.ACTIVIDADES , BPXPORT.SOCIOS
                            WHERE BPXPORT.RESERVAS.ID_ACTIVIDAD = BPXPORT.ACTIVIDADES.id AND BPXPORT.RESERVAS.SOCIO = BPXPORT.SOCIOS.CODIGO $filtro_fecha
                            ORDER BY BPXPORT.ACTIVIDADES.start DESC";
                            
//                           
      $result = mysqli_query($conectando, $sql);

      // comienza un bucle que leerá todos los registros existentes
      while($row = mysqli_fetch_array($result)) {
          $grupoant = $grupo;
          $grupo = $row['id'];

         if($grupoant != $grupo){
             ?>
  
      <tr>
            <td colspan="1" align="center" bgcolor="#f1f1f1"><strong>Actividad: <?php echo $row['title'] ?></strong></td><td colspan="2" align="center" bgcolor="#f1f1f1"><strong>Fecha: <?php echo $row['start'] ?></strong></td><td colspan="1" align="center" bgcolor="#f1f1f1"><strong>Plazas: <?php echo $row['plazas'] ?></strong></td>
      </tr>
<?php   } 
          $fecha_reserva = $row['FECHA'];
          $id_actividad = $row['ID_ACTIVIDAD'];
          $cod_socio = $row['SOCIO'];
          $actividad_confirmada= $row['CONFIRMADA'];
          $nombre_socio = $row['NOMBRE'];
          $apellidos_socio = $row['APELLIDOS'];
          $tipo = '1';
          
          if ($actividad_confirmada == '0'){
              $clase_confirma = "rojo";
          }else {
              $clase_confirma = "verde";
          }
          //capturar la hora de la actividad


  ?>
      <tr class="<?php echo $clase_confirma; ?>" id="linea_reserva"><td colspan="1"><?php echo $cod_socio; ?></td><td colspan="2"><?php echo $nombre_socio." ".$apellidos_socio; ?></td>
<!--          <td><?php texto_confirmada($actividad_confirmada); ?></td>-->
          <td style="float:right;"><i id ="icon-elim-reserva" class="fa fa-trash-alt" style="font-size:15px;" onclick="elimina_reserva(<?php echo $id_actividad?>,<?php echo $cod_socio ?>,<?php echo $tipo ?>)"></i><i id ="icon-confirm-reserva" class="fa fa-check-circle"  style="padding-left:25px;font-size:15px;" onclick="confirma_reserva(<?php echo $id_actividad?>,<?php echo $cod_socio ?>,<?php echo $tipo ?>)"></i><td></tr>                                            
      <!--echo "<div class='linea'><tr class=''><td>$fecha_reserva</td><td>texto_actividad($id_actividad)</td><td>$actividad_confirmada</td></tr</div>";-->
    <?php 

      }

      mysql_free_result($result); // Liberamos los registros
      mysql_close($conectando); // Cerramos la conexion con la base de datos
     ?>
         
  </table>         
</div>

 



</body>
</html>
