<?php
include("funciones.php");
ini_set("session.gc_maxlifetime", 60);

?>
<html>
<head>
 <meta charset="UTF-8">
    <meta name="viewpoort" content="width=device-width,initial-scale=1.0">
    <!--<script type="text/javascript" src="librerias/javascript/jquery.js"></script>-->
    <link rel="stylesheet" href="estilos/estilos.css">
    <link rel="stylesheet" href="css/fontawesome-all.min.css">
    
    
     <!--css que contiene las CSS del autocomplete-->
     <link rel="stylesheet" href="librerias/css/easy-autocomplete.css">
     <link rel="stylesheet" href="librerias/css/easy-autocomplete.themes.css">
    
     <!--librerias bootstrap-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="librerias/javascript/jquery.min.js"></script>
    <script src="librerias/javascript/moment.min.js"></script>
    <script src="librerias/javascript/jquery.min.js"></script>
    <script src="librerias/javascript/moment.min.js"></script>
    
    <script src="librerias/javascript/jquery.easy-autocomplete.min.js"></script> 
    
    <!--full-calendar-->
    <link rel="stylesheet" href="librerias/css/fullcalendar.min.css">
    <script src="librerias/javascript/fullcalendar.min.js"></script>  
    <script src="librerias/javascript/es.js"></script> 
    <!--libreria js-->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
   
    <script language="JavaScript" SRC="funciones.js"></script>
   <!--ClockPickerr-->
   <link rel="stylesheet" href="librerias/css/bootstrap-clockpicker.css">
   <script src="librerias/javascript/bootstrap-clockpicker.js"></script>  
</head>   
<body>   
  <!--modal para visualizar los datos de los socios-->
    <div class="" id="modal_socios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
           <!--Se define el tamaño de la ventana-->
        <div class="modal-dialog modal-lg" role="document">
        <!--Se define estilos de la ventana fondo, bordes, sombreado-->
            <div class="modal-content">
                <!--Se define el boton de cerrar y el titulo-->
                <div class="modal-header">
                    
                    <div class="form-row" style="padding:5px;">
                          
                              <label>Buscar Socio</label>                      
                             <input id="socios_selector" name="socios_selector" style ="margin-left:10px;" onchange="relleno_modal_socio(this)" />       
                          
                         
                      </div>
<!--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><br>
                            <span aria-hidden="true">&times;</span>        
                    </button>-->
                </div>
                
                <!--Se define el contenido del modal-->
                <div class="modal-body">
                    

<!--                    //autocomplete de busqueda de id = socios_selector -->
                    <script type="text/javascript">
                    var options= {
                        url:"autocomplete_socios.php",

                    getValue: function(element) {
                     return element.APELLIDOS + " " + element.NOMBRE + " " + element.CODIGO;

                    },   

                    list:{
                        match:{
                            enabled:true
                        },
                        onClickEvent: function() {                         
                        },
                        onSelectItemEvent: function() {
                            var valor_codigo = jQuery( "#socios_selector" ).getSelectedItemData().CODIGO;
                            var valor_codigopul = jQuery( "#socios_selector" ).getSelectedItemData().CODIGO_PUL;
                            var valor_nombre = jQuery( "#socios_selector" ).getSelectedItemData().NOMBRE;
                            var valor_apellidos = jQuery( "#socios_selector" ).getSelectedItemData().APELLIDOS;
                            var valor_nif = jQuery( "#socios_selector" ).getSelectedItemData().NIF;
                            var valor_movil = jQuery( "#socios_selector" ).getSelectedItemData().MOVIL;
                            var valor_tel1 = jQuery( "#socios_selector" ).getSelectedItemData().TEL1;
                            var valor_tel2 = jQuery( "#socios_selector" ).getSelectedItemData().TEL2;
                            var valor_email = jQuery( "#socios_selector" ).getSelectedItemData().EMAIL;
                            var valor_baja = jQuery( "#socios_selector" ).getSelectedItemData().FECHA_BAJA;
                            jQuery("#txtcodigo").val(valor_codigo); //copy it to the hidden field
                            jQuery("#txtcodigopul").val(valor_codigopul);
                            jQuery("#txtnombre").val(valor_nombre);
                            jQuery("#txtapel").val(valor_apellidos);
                            jQuery("#txtnif").val(valor_nif);
                            jQuery("#txtmovil").val(valor_movil);
                            jQuery("#txttel1").val(valor_tel1);
                            jQuery("#txttel2").val(valor_tel2);
                            jQuery("#txtmail").val(valor_email);
                            jQuery("#txtbaja").val(valor_baja);
                            
                        },


                        maxNumberOfElements:10
                    },
                    theme:"plate-dark"
                };
                $("#socios_selector").easyAutocomplete(options);
                </script>
                
                    <div class="form-row">
                        <div class="form-group col-sm-3">
                            <label>Codigo</label>
                            <input id="txtcodigo" name="txtcodigo" class="form-control" />
                        </div>
                        <div class="form-group col-sm-3">
                            <label>Codigo Pulsera</label>
                            <input id="txtcodigopul" name="txtcodigopul" class="form-control" />
                        </div>
                        
                        
                    </div>
                      
                    <div class="form-row">  
                        <div class="form-group col-sm-3">   
                            <label>Nombre</label>
                            <input id="txtnombre" name="txtnombre" class="form-control" />
                        </div>
                        <div class="form-group col-sm-5">   
                            <label>Apellidos</label>
                            <input id="txtapel" name="txtapel" class="form-control" />
                        </div>
                        <div class="form-group col-sm-2">   
                            <label>Nif</label>
                            <input id="txtnif" name="txtnif" class="form-control" />
                        </div>
                        <div class="form-group col-sm-2">
                            <label>Fecha Baja</label>
                            <input type ="date" id="txtbaja" name="txtbaja" class="form-control" value="" />
                        </div>
                        
                        
                     </div>
                
                    <div class="form-row"> 
                        <div class="form-group col-sm-2">   
                            <label>Movil</label>
                            <input id="txtmovil" name="txtmovil" class="form-control" />
                        </div>
                        <div class="form-group col-sm-2">   
                            <label>Telefono 1</label>
                            <input id="txttel1" name="txttel1" class="form-control" />
                        </div>
                        
                        <div class="form-group col-sm-2">   
                            <label>Telefono 2</label>
                            <input id="txttel2" name="txttel2" class="form-control" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Email</label>
                            <input id="txtmail" name="txtmail" class="form-control" />
                        </div>
                        
                    </div>
                
                    
                
                <label id ="boton_muestra_historial" name="boton_muestra_historial">  Historial<span href="#" id ="flecha_historial" class="fa fa-caret-down"  onclick="mostrar_historial_socio()"></span>  </label>
                   
                    
                    <div id="historial_reservas" name="historial_reservas" display="none"></div> 
                    
                </div>     
                <div class="modal-footer">
                
                <button type="button" class="btn btn-success" id="btnAgregar">Agregar</button>
                <button type="button" class="btn btn-success" id="btnNuevo">Nuevo</button>
                <button type="button" class="btn btn-success" id="btnModificar">Modificar</button>
                <button type="button" class="btn btn-danger" id="btnEliminar">Borrar</button>
              </div>

            </div>
        </div>
    </div>
    
  
      
</body>
<script>

//    La sacamos fuera para poder utilizarla en todo momento 
   
         $("#btnNuevo").click(function(){
          
            $('#calendario_actividades').load('socios.php');
            $("#socios_selector").val($('option:first', select).val());
        });
        $("#btnAgregar").click(function(){
          
            EnviarDatos_Socio('agregar');
        });
        $("#btnEliminar").click(function(){
           
            EnviarDatos_Socio('eliminar');
        });
         $("#btnModificar").click(function(){
           
            EnviarDatos_Socio('modificar');
        });
        
        $("#btnlistado").click(function(){
            
           
           
        });
        
   
         
    function relleno_modal_socio(valor){
//        var seleccionado = $(this).find('option:selected');
        
//       var actividad = seleccionado.data('value');
       
        $('#txtcodigo').val($('option:selected', valor).attr("valor"));
        $('#txtcodigo').val($('option:selected', valor).attr("value"));  
        $('#txtcodigopul').val($('option:selected', valor).attr("data-codpul"));
        $('#txtnombre').val($('option:selected', valor).attr("data-nom"));
        $('#txtapel').val($('option:selected', valor).attr("data-apel"));
        $('#txtnif').val($('option:selected', valor).attr("data-nif"));
        $('#txtmovil').val($('option:selected', valor).attr("data-mov"));   
        $('#txttel1').val($('option:selected', valor).attr("data-tel1"));  
        $('#txttel2').val($('option:selected', valor).attr("data-tel2"));  
        $('#txtmail').val($('option:selected', valor).attr("data-mail")); 
        $('#txtbaja').val($('option:selected', valor).attr("data-baja")); 
                
    }
function EnviarDatos_Socio(accion){
    var codigo = $('#txtcodigo').val();
    var codigo_pul= $('#txtcodigopul').val();
    var nombre = $('#txtnombre').val();
    var apellidos = $('#txtapel').val();
    var nif = $('#txtnif').val();
    var movil = $('#txtmovil').val();
    var tel1 = $('#txttel1').val();
    var tel2 = $('#txttel2').val();
    var mail = $('#txtmail').val();
    var baja = $('#txtbaja').val();
    $.post('ajax.php', {accion_socios: accion,codigo:codigo,codigo_pul:codigo_pul,nombre:nombre,apellidos:apellidos,nif:nif,movil:movil,tel1:tel1,tel2:tel2,mail:mail,baja:baja}, function(mensaje) { 
     alert(mensaje);
      $('#calendario_actividades').load('socios.php');
     $("#socios_selector").val($('option:first', select).val());
    
     
    });
}

function mostrar_historial_socio(valor){
    var valor = valor;
   
    if (!valor){
           
        if($('#flecha_historial').hasClass('fa fa-caret-up')) { 
             $('#historial_reservas').fadeOut('slow');
    //        $('#historial_reservas').css('display','none');
            $('#flecha_historial').removeClass('fa fa-caret-up');
            $('#flecha_historial').addClass('fa fa-caret-down');
            exit();
        }   

        if($('#flecha_historial').hasClass('fa fa-caret-down')) {  
             $('#historial_reservas').fadeIn('slow');
    //        $('#historial_reservas').show();
            $('#flecha_historial').removeClass('fa fa-caret-down');
            $('#flecha_historial').addClass('fa fa-caret-up');
        }   
    }
         
    var codigo_socio = $("#txtcodigo").val();

    $.post('ajax.php', {historial_reservas_socio: codigo_socio}, function(mensaje) { 


        $('#historial_reservas').html(mensaje);


      });      
}
</script>
</html>
