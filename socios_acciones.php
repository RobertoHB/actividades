<?php


header('Content-type: application/json');
$datos = new PDO("mysql:dbname=BPXPORT;host:127.0.0.1","root","eldeorejo");

$accion=(isset($_GET['accion']))?$_GET['accion']:'leer';
switch($accion){
    case 'agregar':
        $sql = $datos->prepare("INSERT INTO 
                SOCIOS(ID,CODIGO,CODIGO_PUL,NOMBRE,APELLIDOS,NIF,MOVIL,TEL1,TEL2,EMAIL,FECHA_BAJA)
                VALUES('',:codigo,:codigo_pul,:nombre,:apellidos,:nif,:movil,:tel1,:tel2,:mail,:baja)");
        $respuesta=$sql->execute(array(
            "CODIGO" => $_POST['codigo'],
            "CODIGO_PUL" => $_POST['codigo_pul'],
            "NOMBRE" => $_POST['nombre'],
            "APELLIDOS" => $_POST['apellidos'],
            "NIF" => $_POST['nif'],
            "MOVIL" => $_POST['movil'],
            "TEL1" => $_POST['tel1'],
            "TEL2" => $_POST['tel2'],
            "EMAIL" => $_POST['mail'],
            "FECHA_BAJA" => $_POST['baja'],
        ));
        echo json_encode($respuesta); 
        break;
    case 'eliminar':
       $respuesta = false;
        if(isset($_POST['codigo'])){
                $sql = $datos->prepare("DELETE FROM BPXPORT.SOCIOS WHERE CODIGO = :codigo");
            $respuesta = $sql->execute(array("codigo"=>$_POST['codigo']));
        }
        echo json_encode($respuesta); 
        break;
    case 'modificar':
        $sql = $datos->prepare("UPDATE BPXPORT.SOCIOS SET
                                
                        
                                CODIGO_PUL=:codigo_pul,
                                NOMBRE=:nombre,
                                APELLIDOS=:apellidos,
                                NIF=:nif,
                                MOVIL=:movil,
                                TEL1=:tel1,
                                TEL2=:tel2,
                                EMAIL=:mail,
                                FECHA_BAJA=:baja
                                WHERE CODIGO = :codigo");
        $respuesta=$sql->execute(array(
//            "codigo" => $_POST['codigo'],
            "codigo_pul" => $_POST['codigo_pul'],
            "nombre" => $_POST['nombre'],
            "apellidos" => $_POST['apellidos'],
            "nif" => $_POST['nif'],
            "movil" => $_POST['movil'],
            "tel1" => $_POST['tel1'],
            "tel2" => $_POST['tel2'],
            "mail" => $_POST['mail'],
            "baja" => $_POST['baja'],
        ));
        echo json_encode($respuesta); 
        break;    
    
}   

?>
