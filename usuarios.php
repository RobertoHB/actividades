<?php
include("funciones.php");
ini_set("session.gc_maxlifetime", 60);

?>
<html>
<head>
 <meta charset="UTF-8">
    <meta name="viewpoort" content="width=device-width,initial-scale=1.0">
    <!--<script type="text/javascript" src="librerias/javascript/jquery.js"></script>-->
    <link rel="stylesheet" href="estilos/estilos.css">
    <link rel="stylesheet" href="css/fontawesome-all.min.css">
    
    
     <!--css que contiene las CSS del autocomplete-->
     <link rel="stylesheet" href="librerias/css/easy-autocomplete.css">
     <link rel="stylesheet" href="librerias/css/easy-autocomplete.themes.css">
    
     <!--librerias bootstrap-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="librerias/javascript/jquery.min.js"></script>
    <script src="librerias/javascript/moment.min.js"></script>
    <script src="librerias/javascript/jquery.min.js"></script>
    <script src="librerias/javascript/moment.min.js"></script>
    
    <script src="librerias/javascript/jquery.easy-autocomplete.min.js"></script> 
    
    <!--full-calendar-->
    <link rel="stylesheet" href="librerias/css/fullcalendar.min.css">
    <script src="librerias/javascript/fullcalendar.min.js"></script>  
    <script src="librerias/javascript/es.js"></script> 
    <!--libreria js-->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
   
    <script language="JavaScript" SRC="funciones.js"></script>
   <!--ClockPickerr-->
   <link rel="stylesheet" href="librerias/css/bootstrap-clockpicker.css">
   <script src="librerias/javascript/bootstrap-clockpicker.js"></script>  
</head>   
<body>   
  <!--modal para visualizar los datos de los socios-->
    <div class="" id="modal_socios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
           <!--Se define el tamaño de la ventana-->
        <div class="modal-dialog" role="document">
        <!--Se define estilos de la ventana fondo, bordes, sombreado-->
            <div class="modal-content">
                <!--Se define el boton de cerrar y el titulo-->
                <div class="modal-header">
                    <h5 class="modal-title" style="color:black">Usuarios</h5>
<!--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><br>
                            <span aria-hidden="true">&times;</span>        
                    </button>-->
                </div>
                
                <!--Se define el contenido del modal-->
                <div class="modal-body">
                    

                      <div class="form-group" style="margin-left:5px;">
                          <label>Buscar Usuario</label><br>
                         
                         <input id="usuarios_selector" name="usuarios_selector" style="width: 280px;" onchange="" />       
                         <!--<input id="codigo_socio" Type="hidden" />-->
                           
                      </div>
                    <script type="text/javascript">
                    var options= {
                        url:"autocomplete_usuarios.php",

                    getValue: function(element) {
                    return element.NOMBRE;

                    },   

                    list:{
                        match:{
                            enabled:true
                        },
                        onClickEvent: function() {                         
                        },
                        onSelectItemEvent: function() {
                            var valor_id = jQuery( "#usuarios_selector" ).getSelectedItemData().ID;
                            var valor_nombre = jQuery( "#usuarios_selector" ).getSelectedItemData().NOMBRE;
                            var valor_contra = jQuery( "#usuarios_selector" ).getSelectedItemData().CONTRA;
                            var valor_tipo = jQuery( "#usuarios_selector" ).getSelectedItemData().TIPO;
                            
                            jQuery("#txtnombre_usu").val(valor_nombre); //copy it to the hidden field
                            jQuery("#txttipo_usu").val(valor_tipo);
                            jQuery("#txtcontra_usu").val(valor_contra);
                            jQuery("#txtid_usu").val(valor_id);
                            
                        },


                        maxNumberOfElements:4
                    },
                    theme:"plate-dark"
                };
                $("#usuarios_selector").easyAutocomplete(options);
                </script>
                    <input id="txtid_usu" name="txtid_usu" type="hidden" />
                    <div class="form-row">
                        <div class="form-group col-sm-6">
                            <label>Nombre</label>
                            <input id="txtnombre_usu" name="txtnombre_usu" class="form-control" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Tipo</label>
                            <input id="txttipo_usu" name="txttipo_usu" class="form-control" />
                        </div>
                    </div>
                      
                    <div class="form-row">  
                        <div class="form-group col-sm-3">   
                            <label>Contraseña</label>
                            <input id="txtcontra_usu" name="txtcontra_usu" type = "password" class="form-control" />
                        </div>
                        <div class="form-group col-sm-3">   
                          
                            <input id="txtnuevacontra" name="txtnuevacontra" class="form-control" type = "password" placeholder="Nueva" style="margin-top: 32px;height: 38px;font-size:13px;color:#269abc"/>
                        </div>
                        <div class="form-group col-sm-3">   
                           
                            <input id="txtconfirmacontra" name="txtconfirmacontra" class="form-control" type = "password" placeholder="Confirmar" style="margin-top: 32px;height: 38px;font-size:13px;color:#269abc" onchange="valida_nueva_contra()"/>
                            
                        </div>
                        <div class="form-group col-sm-3" id = "ico_valida_contra" style ="margin-top:42px;padding-left:5px;font-size:25px;display:none" >   
                        <span class="fa fa-check" id="ico_valida_contra_usu" onclick="valida_nueva_contra()"></span>
                        
                        </div>
                   
                </div>     
                <div class="modal-footer">
                <button type="button" class="btn btn-success" id="btnAgregar">Agregar</button>
                <button type="button" class="btn btn-success" id="btnModificar">Modificar</button>
                <button type="button" class="btn btn-danger" id="btnEliminar">Borrar</button>
              </div>

            </div>
        </div>
    </div>
    
  
      
</body>
<script>

//    La sacamos fuera para poder utilizarla en todo momento 
   

        $("#btnAgregar").click(function(){
          
            EnviarDatos_usuario('agregar');
        });
        $("#btnEliminar").click(function(){
           
            EnviarDatos_usuario('eliminar');
        });
         $("#btnModificar").click(function(){
           
            EnviarDatos_usuario('modificar');
        });
                                                   
         
   
function EnviarDatos_usuario(accion){
    var id = $('#txtid_usu').val();
    var nombre = $('#txtnombre_usu').val();
    var contra= $('#txtcontra_usu').val();
    var tipo = $('#txttipo_usu').val();
    
    $.post('ajax.php', {accion_usuarios: accion,id:id,nombre:nombre,contra:contra,tipo:tipo}, function(mensaje) { 
     alert(mensaje);
     
    });
}

function valida_nueva_contra(){
    var nueva_contra = $('#txtnuevacontra').val();
    var confirma_contra = $('#txtconfirmacontra').val();
    if (confirma_contra == nueva_contra){
        $('#ico_valida_contra').show();
        $('#ico_valida_contra').css("color", "green");
        $('#txtcontra_usu').val(confirma_contra);
        
    }else{
         $('#ico_valida_contra').css("color", "red");
         alert("Confirmación de contraseña incorrecta.")
    }
}

</script>
</html>
